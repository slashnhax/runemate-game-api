package com.runemate.game.api.hybrid.local.hud.interfaces;

import com.runemate.client.game.open.*;
import com.runemate.game.api.hybrid.entities.*;
import com.runemate.game.api.hybrid.entities.definitions.*;
import com.runemate.game.api.hybrid.entities.details.*;
import com.runemate.game.api.hybrid.input.*;
import com.runemate.game.api.hybrid.local.hud.Menu;
import com.runemate.game.api.hybrid.local.hud.*;
import com.runemate.game.api.hybrid.util.*;
import com.runemate.game.api.osrs.local.hud.interfaces.*;
import com.runemate.game.api.script.framework.core.*;
import com.google.common.cache.*;
import java.awt.*;
import java.awt.image.*;
import java.time.*;
import java.util.regex.*;
import javafx.scene.canvas.*;
import lombok.*;
import lombok.extern.log4j.*;
import org.jetbrains.annotations.*;

/**
 * An item represented by a Sprite, typically on a InterfaceComponent
 */
@Log4j2
public class SpriteItem implements Item, Interactable, Validatable, Renderable {

    private final int id, quantity, index;
    private final Origin origin;

    public SpriteItem(final int id, final int quantity) {
        this(id, quantity, -1, Origin.UNKNOWN);
    }

    public SpriteItem(final int id, final int quantity, final int index, final Origin origin) {
        if (origin == null) {
            throw new IllegalArgumentException(
                "Origin cannot be null, pass Origin.UNKNOWN instead.");
        }
        this.id = id;
        this.quantity = quantity;
        this.index = index;
        this.origin = origin;
    }

    /**
     * Derives a pseudo SpriteItem from this SpriteItem
     */
    public SpriteItem derive(final int quantity) {
        return new SpriteItem(id, this.quantity + quantity, index, origin);
    }

    @Nullable
    public InteractableRectangle getBounds() {
        switch (origin) {
            case BANK:
                return Bank.getBoundsOf(index);
            case EQUIPMENT:
                return Equipment.getBoundsOf(Equipment.Slot.resolve(index));
            case INVENTORY:
                return Inventory.getBoundsOf(index);
            case SHOP:
                return Shop.getBoundsOf(index);
            case SEED_VAULT:
                return SeedVault.getBoundsOf(index);
            case OUTGOING_TRADE_OFFER:
                return Trade.Outgoing.getBoundsOf(index);
            case INCOMING_TRADE_OFFER:
                return Trade.Incoming.getBoundsOf(index);
            default:
                log.warn("getBounds() not possible - unsupported origin {}", origin);
                return null;
        }
    }

    @Override
    public int getId() {
        return id;
    }

    @Override
    public int getQuantity() {
        return quantity;
    }

    @Override
    @Nullable
    public ItemDefinition getDefinition() {
        return ItemDefinition.get(id);
    }

    public int getIndex() {
        return index;
    }

    public Origin getOrigin() {
        return origin;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + quantity;
        result = 31 * result + index;
        result = 31 * result + origin.hashCode();
        return result;
    }

    @Override
    public boolean equals(final Object object) {
        if (object instanceof SpriteItem) {
            final SpriteItem item = (SpriteItem) object;
            return id == item.id && index == item.index && origin == item.origin &&
                quantity == item.quantity;
        }
        return false;
    }

    @Override
    public String toString() {
        if (BotPlatform.isBotThread()) {
            ItemDefinition def = getDefinition();
            return "SpriteItem(name:" + (def != null ? def.getName() : "N/A (" + id + ")") +
                ", quantity: " + quantity + ", index: " + index + ",  origin: " + origin + ")";
        }
        return "SpriteItem(" + id + " x " + quantity + ", index: " + index + ",  origin: " +
            origin + ")";
    }

    @Override
    public boolean isValid() {
        return switch (origin) {
            case BANK -> equals(Bank.getItemIn(index));
            case EQUIPMENT -> equals(Equipment.getItemIn(Equipment.Slot.resolve(index)));
            case INVENTORY -> equals(Inventory.getItemIn(index));
            case SHOP -> equals(Shop.getItemIn(index));
            case SEED_VAULT -> equals(SeedVault.getItemIn(index));
            case OUTGOING_TRADE_OFFER -> equals(Trade.Outgoing.getItemIn(index));
            case INCOMING_TRADE_OFFER -> equals(Trade.Incoming.getItemIn(index));
            default -> {
                log.warn("isValid() not possible - unsupported origin {}", origin);
                yield false;
            }
        };
    }

    @Override
    public boolean isVisible() {
        final Rectangle bounds = getBounds();
        if (bounds != null) {
            InteractableRectangle viewport;
            switch (origin) {
                case BANK:
                    final var currentTab = Bank.getCurrentTab();
                    return Bank.isOpen()
                        && ((viewport = Bank.getViewport()) != null
                        && Interfaces.isVisibleInScrollpane(bounds, viewport))
                        && (currentTab == 0 || currentTab == Bank.getTabContaining(this));
                case EQUIPMENT:
                    return InterfaceWindows.getEquipment().isOpen()
                        && ((viewport = Equipment.getViewport()) == null || viewport.contains(bounds));
                case INVENTORY:
                    return InterfaceWindows.getInventory().isOpen()
                        && ((viewport = Inventory.getViewport()) == null || viewport.contains(bounds));
                case SHOP:
                    if (Shop.isOpen()) {
                        InteractableRectangle itemBounds = Shop.getBoundsOf(index);
                        if (itemBounds != null) {
                            viewport = Shop.getViewport();
                            return viewport == null || viewport.contains(itemBounds);
                        }
                    }
                    return false;
                case OUTGOING_TRADE_OFFER:
                case INCOMING_TRADE_OFFER:
                    return Trade.atOfferScreen();
                case LOOTING_BAG:
                    return LootingBag.isOpen();
                case COLLECTION_BOX:
                case SEED_VAULT:
                    return SeedVault.isOpen()
                        && (viewport = SeedVault.getViewport()) != null
                        && viewport.contains(bounds);
                case PLAYER_APPEARANCE://Should this just always return true...? It may be RS3 only since it's cosmetic...
                case UNKNOWN:
                    break;
            }
        }
        log.warn("isVisible() not possible - unsupported origin {}", origin);
        return false;
    }

    @Override
    public double getVisibility() {
        return isVisible() ? 100 : 0;
    }

    @Override
    public boolean hasDynamicBounds() {
        return false;
    }

    @Override
    @Nullable
    public InteractablePoint getInteractionPoint(Point point) {
        final InteractableRectangle bounds = getBounds();
        return bounds != null ? bounds.getInteractionPoint(point) : null;
    }

    @Override
    public boolean contains(final Point point) {
        final Rectangle bounds = getBounds();
        return bounds != null && bounds.contains(point);
    }

    @Override
    public boolean click() {
        return (isVisible() || display()) && Mouse.click(this, Mouse.Button.LEFT);
    }

    @Override
    public boolean hover() {
        return (isVisible() || display()) && Mouse.move(this);
    }

    @Override
    public boolean interact(final Pattern action, final Pattern target) {
        return (isVisible() || display()) && Menu.click(this, action, target);
    }

    @Override
    public void render(Graphics2D g2d) {
        final Rectangle bounds = getBounds();
        if (bounds != null && isVisible()) {
            g2d.drawRect(bounds.x, bounds.y, bounds.width, bounds.height);
        }
    }

    @Override
    public void render(GraphicsContext gc) {
        final Rectangle bounds = getBounds();
        if (bounds != null && isVisible()) {
            gc.strokeRect(bounds.x, bounds.y, bounds.width, bounds.height);
        }
    }

    private boolean display() {
        InteractableRectangle viewport;
        switch (origin) {
            case BANK:
                OSRSBank.openTabContaining(this);
                viewport = Bank.getViewport();
                break;
            case EQUIPMENT:
                InterfaceWindows.getEquipment().open();
                viewport = Equipment.getViewport();
                break;
            case INVENTORY:
                InterfaceWindows.getInventory().open();
                viewport = Inventory.getViewport();
                break;
            case SHOP:
                viewport = Shop.getViewport();
                break;
            case SEED_VAULT:
                viewport = SeedVault.getViewport();
                break;
            default:
                viewport = null;
                log.warn("display() not possible - unsupported origin {}", origin);
        }
        return (viewport == null || Interfaces.scrollTo(this, viewport)) && isVisible();
    }

    public Image getImage() {
        return new Image(this);
    }

    /**
     * Represents the various states an item can take (quantity/stack size), and loads image variations based on those states.
     */
    @RequiredArgsConstructor
    public static class Image {

        private static final int DEFAULT_WIDTH = 36;
        private static final int DEFAULT_HEIGHT = 32;
        private static final Cache<Key, BufferedImage> IMAGE_CACHE = CacheBuilder.newBuilder()
            .expireAfterAccess(Duration.ofHours(1))
            .build();

        private final SpriteItem item;

        /**
         * Gets a BufferedImage representing the item in its current state (aka. current stack size).
         */
        public BufferedImage get() {
            return load(new Key(item.id, item.quantity, item.quantity > 1));
        }

        /**
         * Gets a BufferedImage representing the item at the provided {@code quantity}
         */
        public BufferedImage get(int quantity) {
            return load(new Key(item.id, quantity, quantity > 1));
        }

        /**
         * Gets a BufferedImage representing the item at the provided {@code quantity} and {@code stacks}.
         */
        public BufferedImage get(int quantity, boolean stacks) {
            return load(new Key(item.id, quantity, stacks));
        }

        @Nullable
        private BufferedImage load(@NonNull Key key) {
            try {
                var image = IMAGE_CACHE.getIfPresent(key);
                if (image != null) {
                    return image;
                }

                final var pixels = OpenClient.getSpritePixels(key.id, Math.max(0, key.quantity));
                if (pixels == null) {
                    return null;
                }

                final int[] transPixels = new int[pixels.length];
                for (int i = 0; i < transPixels.length; i++) {
                    if (pixels[i] != 0) {
                        transPixels[i] = pixels[i] | 0xff000000;
                    }
                }

                final var buffered = new BufferedImage(DEFAULT_WIDTH, DEFAULT_HEIGHT, BufferedImage.TYPE_INT_ARGB);
                buffered.setRGB(0, 0, DEFAULT_WIDTH, DEFAULT_HEIGHT, transPixels, 0, DEFAULT_WIDTH);
                IMAGE_CACHE.put(key, buffered);
                return buffered;
            } catch (Exception e) {
                log.warn("Failed to load image for {}", item, e);
            }
            return null;
        }

        @Value
        private static class Key {

            int id;
            int quantity;
            boolean stackable;
        }
    }

    public enum Origin {
        UNKNOWN,
        BANK,
        GROUP_BANK,
        SEED_VAULT,
        @Deprecated BEAST_OF_BURDEN,
        EQUIPMENT,
        INVENTORY,
        GRAND_EXCHANGE,
        REWARDS_CHEST,
        LOOTING_BAG,
        SHOP,
        @Deprecated SHOP_FREE,
        OUTGOING_TRADE_OFFER,
        INCOMING_TRADE_OFFER,
        COLLECTION_BOX,
        PLAYER_APPEARANCE,
        @Deprecated MONEY_POUCH,
        TOA_RAID_SHOP,
        TOA_RAID_SUPPLIES
    }
}
