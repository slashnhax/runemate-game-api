package com.runemate.ui.control;

import com.runemate.game.api.hybrid.*;
import com.runemate.game.api.hybrid.entities.definitions.*;
import com.runemate.game.api.hybrid.local.hud.interfaces.*;
import com.runemate.game.api.script.framework.core.*;
import com.runemate.ui.control.setting.*;
import com.sun.javafx.tk.*;
import java.util.*;
import javafx.event.*;
import javafx.geometry.*;
import javafx.scene.control.*;
import javafx.scene.layout.*;
import javafx.scene.text.*;
import lombok.*;
import lombok.extern.log4j.*;

@Log4j2(topic = "EquipmentDialog")
public class ModalEquipmentOverlay extends VBox {

    @Getter
    private final ModalOverlay<EquipmentLoadout> overlay;

    public ModalEquipmentOverlay(BotPlatform bot, Settings.Setting setting, @NonNull EquipmentLoadout existing) {
        setSpacing(8);
        setAlignment(Pos.TOP_CENTER);
        setPadding(new Insets(4, 12, 12, 12));

        Text text = new Text(setting.setting().setting().title());
        text.setFont(Font.font("", FontWeight.BOLD, 16));
        getChildren().add(text);

        List<ItemDefinition> candidates = null;
        try {
            candidates = bot.invoke(() -> ItemDefinition.loadAll(existing.loadout()));
        } catch (Exception e) {
            log.warn("Failed to look up existing ItemDefinitions", e);
        }

        final Map<Equipment.Slot, SlotControl> slots = new EnumMap<>(Equipment.Slot.class);
        for (Equipment.Slot slot : Equipment.Slot.values()) {
            ItemDefinition initial = candidates != null ? existing.getDefinition(slot, candidates) : null;
            if (initial != null) {
                slots.put(slot, new SlotControl(bot, slot, initial));
            } else {
                slots.put(slot, new SlotControl(bot, slot));
            }
        }

        GridPane grid = new GridPane();
        grid.setAlignment(Pos.TOP_CENTER);
        ColumnConstraints hConstraints = new ColumnConstraints(36, 36, 36);
        RowConstraints vConstraints = new RowConstraints(36, 36, 36);
        grid.setVgap(1);
        grid.setHgap(1);
        grid.getColumnConstraints().add(hConstraints);
        grid.getRowConstraints().add(vConstraints);
        grid.setPadding(new Insets(4));
        grid.add(slots.get(Equipment.Slot.HEAD), 1, 0);
        grid.add(slots.get(Equipment.Slot.CAPE), 0, 1);
        grid.add(slots.get(Equipment.Slot.NECK), 1, 1);
        grid.add(slots.get(Equipment.Slot.AMMUNITION), 2, 1);
        grid.add(slots.get(Equipment.Slot.WEAPON), 0, 2);
        grid.add(slots.get(Equipment.Slot.BODY), 1, 2);
        grid.add(slots.get(Equipment.Slot.SHIELD), 2, 2);
        grid.add(slots.get(Equipment.Slot.LEGS), 1, 3);
        grid.add(slots.get(Equipment.Slot.HANDS), 0, 4);
        grid.add(slots.get(Equipment.Slot.FEET), 1, 4);
        grid.add(slots.get(Equipment.Slot.RING), 2, 4);

        getChildren().add(grid);

        Button refresh = new Button("Refresh");
        refresh.setOnAction(e -> slots.values().forEach(SlotControl::refresh));
        refresh.setPrefWidth(110);

        Button clear = new Button("Clear");
        clear.setOnAction(e -> slots.values().forEach(SlotControl::clear));
        clear.setPrefWidth(110);

        Button save = new Button("Save");
        save.getStyleClass().add("accented-button");
        save.setPrefWidth(110);

        overlay = new ModalOverlay<>(this);

        save.setOnAction(e -> {
            e.consume();

            EquipmentLoadout loadout = new EquipmentLoadout();
            for (Map.Entry<Equipment.Slot, SlotControl> slot : slots.entrySet()) {
                loadout.put(slot.getKey(), slot.getValue().getPattern());
            }
            overlay.close(e, loadout);
        });

        getChildren().addAll(refresh, clear, save);
    }
}
