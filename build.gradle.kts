import org.apache.tools.ant.filters.ReplaceTokens

plugins {
    java
    idea
    `java-library`
    `maven-publish`
    id("io.freefair.lombok") version "6.3.0"
    id("org.jetbrains.dokka") version "1.6.10"
    id("com.github.johnrengelman.shadow") version "7.1.2"
    id("org.openjfx.javafxplugin") version "0.0.10"
}

group = "com.runemate"
version = "1.24.1"

val docs: Configuration by configurations.creating
val runemate: Configuration by configurations.creating {
    configurations["compileOnly"].extendsFrom(this)
    configurations["testCompileOnly"].extendsFrom(this)
    configurations["testImplementation"].extendsFrom(this)
}

repositories {
    maven("https://gitlab.com/api/v4/projects/10471880/packages/maven")
    mavenCentral()
    mavenLocal()
}

dependencies {
    val bom = platform("com.runemate:runemate-client-bom:4.2.1.0")
    runemate(bom)

    runemate("com.runemate:runemate-client")
    runemate("com.google.code.gson:gson")
    runemate("org.jetbrains:annotations")
    runemate("com.google.guava:guava")
    runemate("org.apache.commons:commons-lang3")
    runemate("org.apache.commons:commons-math3:")
    runemate("org.apache.commons:commons-text")
    runemate("commons-io:commons-io")
    runemate("com.fasterxml.jackson.module:jackson-module-kotlin")
    runemate("com.fasterxml.jackson.datatype:jackson-datatype-jsr310")
    runemate("com.fasterxml.jackson.datatype:jackson-datatype-jdk8")

    docs(bom)
    docs(group = "com.runemate", name = "runemate-client", classifier = "javadoc")

    implementation("org.json:json:20211205")
    implementation("org.jblas:jblas:1.2.5")
}

val javafxVersion = "20"

javafx {
    version = javafxVersion
    configuration = "runemate"
    modules("javafx.base", "javafx.fxml", "javafx.controls", "javafx.media", "javafx.web", "javafx.graphics", "javafx.swing")
}

java {
    sourceCompatibility = JavaVersion.VERSION_17
    targetCompatibility = JavaVersion.VERSION_17
    withSourcesJar()
    withJavadocJar()
}

gradle.taskGraph.whenReady {
    if (hasTask("launch")) {
        tasks.withType<Javadoc>().forEach { it.enabled = false }
    }
}

tasks {
    withType<JavaCompile> {
        options.encoding = "UTF-8"
    }

    withType<Javadoc> {
        exclude("**/README.md")
        title = "RuneMate Game API $version"
        source = sourceSets.main.get().allJava

        options {
            showFromPublic()
            encoding = "UTF-8"
            (this as CoreJavadocOptions).addStringOption("Xdoclint:none", "-quiet")
        }
    }

    register<Jar>("testJar") {
        group = "build"
        from(sourceSets.test.get().output)
        archiveClassifier.set("test")
    }

    register<Copy>("combinedJavadoc") {
        dependsOn("javadoc")
        from(layout.projectDirectory.file(".gitlab/index.html"))
        from(zipTree(docs.singleFile)) {
            into("runemate-client")
        }
        from(layout.buildDirectory.dir("docs/javadoc")) {
            into("runemate-game-api")
        }
        destinationDir = file(layout.buildDirectory.dir("public"))
    }

    register("launch", JavaExec::class) {
        group = "runemate"
        classpath = files(runemate, shadowJar)
        mainClass.set("com.runemate.client.boot.Boot")

        arrayOf(
                "java.base/java.lang.reflect",
                "java.base/java.nio",
                "java.base/sun.nio.ch",
                "java.base/java.util.regex",
                "java.base/java.util.concurrent",
        ).forEach {
            jvmArgs("--add-opens=${it}=ALL-UNNAMED")
        }
    }

    processResources {
        val tokens = mapOf(
                "runemate.version" to project.version
        )

        inputs.properties(tokens)

        filesMatching("runemate-game-api.version") {
            filter(ReplaceTokens::class, "tokens" to tokens)
            filteringCharset = "UTF-8"
        }

        filesMatching("**/*.fxml") {
            filter { it.replace(Regex("http://javafx.com/javafx/([0-9.]+)"), "http://javafx.com/javafx/$javafxVersion") }
            filteringCharset = "UTF-8"
        }
    }
}

val externalRepositoryUrl: String by project
val externalRepositoryCredentialsName: String? by project
val externalRepositoryCredentialsValue: String? by project

publishing {
    publications {
        register<MavenPublication>("maven") {
            from(components["java"])
        }
    }

    repositories.maven {
        name = "external"
        url = uri(externalRepositoryUrl)
        if (externalRepositoryCredentialsValue != null) {
            credentials(HttpHeaderCredentials::class) {
                name = externalRepositoryCredentialsName ?: "Private-Token"
                value = externalRepositoryCredentialsValue
            }
            authentication.create<HttpHeaderAuthentication>("header")
        }
    }
}
