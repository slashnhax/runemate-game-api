package com.runemate.game.api.hybrid.cache.elements;

import com.runemate.game.api.hybrid.structures.*;
import com.runemate.game.cache.io.*;
import java.io.*;

public class LandscapeObjectPlacementInfo {
    public int orientation;
    public int shape;
    public CoordinateSpace position;

    public LandscapeObjectPlacementInfo(Js5InputStream stream) throws IOException {
        this(stream, false);
    }

    public LandscapeObjectPlacementInfo(Js5InputStream stream, int info, boolean hasProperties)
        throws IOException {
        this(stream, info, hasProperties, true);
    }

    LandscapeObjectPlacementInfo(
        Js5InputStream stream, int info, boolean hasProperties,
        boolean parseCoordinateSpaceAllAtOnce
    ) throws IOException {
        boolean hasCoordinateSpaceInfo = 0 != (info & 0x80);
        if (!hasProperties) {
            this.shape = (info >> 2 & 0x1F);
            this.orientation = (info & 3);
        } else {
            int n3 = stream.readUnsignedByte();
            this.shape = (info & 0x7F);
            this.orientation = n3;
        }
        this.position = null;
        if (hasCoordinateSpaceInfo) {
            this.position = new CoordinateSpace(stream, parseCoordinateSpaceAllAtOnce);
        }
    }

    LandscapeObjectPlacementInfo(Js5InputStream stream, boolean hasProperties) throws IOException {
        this(stream, hasProperties, true);
    }

    LandscapeObjectPlacementInfo(Js5InputStream stream, boolean hasProperties, boolean bl3)
        throws IOException {
        this(stream, stream.readUnsignedByte(), hasProperties, bl3);
    }
}
