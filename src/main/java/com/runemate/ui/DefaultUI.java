package com.runemate.ui;

import com.runemate.game.api.client.embeddable.*;
import com.runemate.game.api.hybrid.*;
import com.runemate.game.api.hybrid.util.*;
import com.runemate.game.api.script.framework.*;
import com.runemate.game.api.script.framework.listeners.events.*;
import com.runemate.game.internal.*;
import com.runemate.ui.control.*;
import com.runemate.ui.tracker.*;
import java.util.*;
import java.util.concurrent.*;
import javafx.animation.*;
import javafx.application.*;
import javafx.beans.property.*;
import javafx.concurrent.*;
import javafx.event.*;
import javafx.scene.*;
import javafx.scene.control.*;
import javafx.util.*;
import lombok.*;
import lombok.experimental.*;
import lombok.extern.log4j.*;
import org.jetbrains.annotations.*;

@Log4j2(topic = "RuneMate")
@Accessors(fluent = true)
public class DefaultUI implements EmbeddableUI {

    @Getter
    private final AbstractBot bot;
    @Getter
    private final StopWatch runtime = new StopWatch();
    @Getter
    private final LongProperty runtimeProperty = new SimpleLongProperty(0);

    ObjectProperty<ControlPanel> controlPanel;
    Timeline timeline;

    public DefaultUI(@NonNull final AbstractBot bot) {
        this.bot = bot;
        this.runtime.start();

        timeline = new Timeline(new KeyFrame(Duration.millis(100)));
        controlPanel = new SimpleObjectProperty<>(new ControlPanel(this));

        //Update stopwatch task
        addRepeatTask(() -> {
            runtimeProperty().set(runtime().getRuntime());
            if (bot().isStopped()) {
                timeline.stop();
            }
        });
        //Update bot status task
        addRepeatTask(() -> {
            final String text = (String) bot.getConfiguration().get("bot.status");
            if (text != null) {
                controlPanel.get().setStatusText(text);
            }
        });

        timeline.setCycleCount(Animation.INDEFINITE);
        timeline.play();
    }

    @InternalAPI
    public void addRepeatTask(@NonNull Runnable runnable) {
        timeline.getKeyFrames().add(new KeyFrame(Duration.ZERO, e -> {
            try {
                runnable.run();
            } catch (Throwable t) {
                log.warn("Error when executing repeat task", t);
            }
        }));
    }

    @Override
    @InternalAPI
    public ObjectProperty<ControlPanel> botInterfaceProperty() {
        return controlPanel;
    }

    private static @Nullable ControlPanel getControlPanel(@NonNull AbstractBot bot) {
        EmbeddableUI embeddable = bot.getEmbeddableUI();
        return embeddable instanceof DefaultUI ui ? ui.botInterfaceProperty().get() : null;
    }

    public static void addPanel(@NonNull String title, @NonNull Node content) {
        addPanel(-1, title, content);
    }

    public static void addPanel(int index, @NonNull String title, @NonNull Node content) {
        final AbstractBot bot = Environment.getBot();
        if (bot != null) {
            addPanel(index, bot, title, content);
        }
    }

    public static void addPanel(@NonNull AbstractBot bot, @NonNull String title, @NonNull Node content) {
        addPanel(-1, bot, title, content, false);
    }

    public static void addPanel(int index, @NonNull AbstractBot bot, @NonNull String title, @NonNull Node content) {
        addPanel(index, bot, title, content, false);
    }

    public static void addPanel(@NonNull AbstractBot bot, @NonNull String title, @NonNull Node content, boolean expanded) {
        addPanel(-1, bot, title, content, expanded);
    }

    public static void addPanel(int index, @NonNull AbstractBot bot, @NonNull String title, @NonNull Node content, boolean expanded) {
        ControlPanel cp = getControlPanel(bot);
        if (cp == null) {
            if (Environment.isDevMode()) {
                throw new IllegalStateException("Unable to resolve DefaultUI ControlPanel, current thread: " + Thread.currentThread().getName());
            } else {
                log.warn("Unable to resolve DefaultUI Control Panel when adding {}", content);
                return;
            }
        }
        Platform.runLater(() -> {
            TitledPane pane = new TitledPane(title, content);
            pane.setExpanded(expanded);

            List<Node> children = cp.getContentContainer().getChildren();
            children.add(index < 0 ? children.size() : index, pane);
        });
    }

    public static void setStatus(@NonNull String status) {
        final AbstractBot bot = Environment.getBot();
        if (bot != null) {
            setStatus(bot, status);
        }
    }

    public static void setStatus(@NonNull AbstractBot bot, @NonNull String status) {
        bot.getConfiguration().put("bot.status", status);
    }

    public static void setItemEventFilter(@NonNull AbstractBot bot, @NonNull ItemEventFilter filter) {
        final ControlPanel panel = getControlPanel(bot);
        if (panel != null) {
            panel.getLootPane().setItemEventFilter(filter);
        }
    }

    public static void setItemEventListening(@NonNull AbstractBot bot, boolean enabled) {
        final ControlPanel panel = getControlPanel(bot);
        if (panel != null) {
            panel.getLootPane().setItemEventListening(enabled);
        }
    }

    public static void submitItemEvent(@NonNull AbstractBot bot, final ItemEvent event) {
        final ControlPanel panel = getControlPanel(bot);
        if (panel != null) {
            panel.getLootPane().submitItemEvent(event);
        }
    }

    /**
     * Displays the node as an overlay, blocking until {@link ModalOverlay#getResult()} is available.
     * <p>
     * By default, this is only as a result of the top-right close button being clicked, in which case a null result is returned. Custom
     * nodes may change this by providing a result to {@link ModalOverlay#close(ActionEvent, Object)}.
     * <pre>
     *     {@code
     *      Button button = new Button("I return true!");
     *      ModalOverlay<Boolean> overlay = new ModalOverlay<>(overlay);
     *      button.setOnAction(event -> overlay.close(event, true));
     *      Boolean result = DefaultUI.showModalOverlay(bot, overlay);
     *     }
     * </pre>
     */
    public static <T> T showModalOverlay(@NonNull AbstractBot bot, Node node) {
        final ControlPanel panel = getControlPanel(bot);
        if (panel == null) {
            return null;
        }
        ModalOverlay<T> overlay = new ModalOverlay<>(node);
        return showModalOverlay(bot, overlay);
    }

    /**
     * Displays the node wrapped by the modal as an overlay, blocking until {@link ModalOverlay#getResult()} is available.
     * <p>
     * By default, this is only as a result of the top-right close button being clicked, in which case a null result is returned. Custom
     * nodes may change this by providing a result to {@link ModalOverlay#close(ActionEvent, Object)}.
     * <pre>
     *     {@code
     *      Button button = new Button("I return true!");
     *      ModalOverlay<Boolean> overlay = new ModalOverlay<>(overlay);
     *      button.setOnAction(event -> overlay.close(event, true));
     *      Boolean result = DefaultUI.showModalOverlay(bot, overlay);
     *     }
     * </pre>
     */
    public static <T> T showModalOverlay(@NonNull AbstractBot bot, ModalOverlay<T> overlay) {
        final ControlPanel panel = getControlPanel(bot);
        if (panel == null) {
            return null;
        }

        panel.getPrimaryStackPane().getChildren().add(overlay);
        return overlay.getResult();
    }

}
