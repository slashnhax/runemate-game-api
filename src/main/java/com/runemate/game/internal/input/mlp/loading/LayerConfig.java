package com.runemate.game.internal.input.mlp.loading;

import com.runemate.game.internal.input.mlp.activation.ActivationFunction;
import com.runemate.game.internal.input.mlp.activation.impl.Relu;
import com.runemate.game.internal.input.mlp.layers.Layer;
import com.runemate.game.internal.input.mlp.layers.impl.DenseLayer;
import org.jblas.DoubleMatrix;

public class LayerConfig {

    private final String type, activation;
    private final int size;
    private DoubleMatrix weights, biases;

    public LayerConfig(String type, String activation, int size) {
        this.type = type;
        this.activation = activation;
        this.size = size;
    }

    public String getType() {
        return type;
    }

    public int getSize() {
        return size;
    }

    public void setBiases(DoubleMatrix biases) {
        this.biases = biases;
    }

    public void setWeights(DoubleMatrix weights) {
        this.weights = weights;
    }

    /**
     * Builds a layer based on the configuration
     *
     * @return the layer
     */
    public Layer buildLayer() {
        final ActivationFunction activationFunction = new Relu();
        return new DenseLayer(this.weights, this.biases, activationFunction);
    }

}
