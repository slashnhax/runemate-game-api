package com.runemate.game.api.hybrid.util;

/**
 * An entity that can be verified to exist
 */
public interface Validatable {
    /**
     * Verifies whether this entity is valid and still exists, typically on the world-graph
     *
     * @return true if it's still available, otherwise false
     */
    boolean isValid();
}
