package com.runemate.game.api.hybrid.region;

import com.runemate.game.api.hybrid.entities.*;
import com.runemate.game.api.hybrid.location.*;
import com.runemate.game.api.hybrid.queries.*;
import com.runemate.game.api.hybrid.queries.results.*;
import com.runemate.game.api.hybrid.util.*;
import com.runemate.game.api.osrs.region.*;
import java.util.function.*;
import java.util.regex.*;

/**
 * For retrieval, sorting, and analysis of GroundItems
 */
public final class GroundItems {
    private GroundItems() {
    }

    public static GroundItemQueryBuilder newQuery() {
        return new GroundItemQueryBuilder();
    }

    /**
     * Gets all loaded GroundItems
     */
    public static LocatableEntityQueryResults<GroundItem> getLoaded() {
        return getLoaded((Predicate<? super GroundItem>) null);
    }

    /**
     * Gets all loaded GroundItems that have one of the given ids
     */
    public static LocatableEntityQueryResults<GroundItem> getLoaded(final int... ids) {
        return getLoaded(Items.getIdPredicate(ids));
    }

    /**
     * Gets all loaded GroundItems that have one of the given names
     */
    public static LocatableEntityQueryResults<GroundItem> getLoaded(final String... names) {
        return getLoaded(Items.getNamePredicate(names));
    }

    /**
     * Gets all loaded GroundItems that are accepted by the filter
     */
    public static LocatableEntityQueryResults<GroundItem> getLoaded(final Predicate<? super GroundItem> filter) {
        return OSRSGroundItems.getLoaded(filter);
    }

    /**
     * Gets all loaded GroundItems at the specified position
     */
    public static LocatableEntityQueryResults<GroundItem> getLoadedOn(final Coordinate position) {
        return getLoadedOn(position, (Predicate<? super GroundItem>) null);
    }

    /**
     * Gets all loaded GroundItems at the specified position that are accepted by the filter
     */
    public static LocatableEntityQueryResults<GroundItem> getLoadedOn(
        final Coordinate position,
        final Predicate<? super GroundItem> filter
    ) {
        return OSRSGroundItems.getLoadedOn(position, filter);
    }

    public static LocatableEntityQueryResults<GroundItem> getLoadedOn(
        final Coordinate position,
        final int... ids
    ) {
        return getLoadedOn(position, Items.getIdPredicate(ids));
    }

    public static LocatableEntityQueryResults<GroundItem> getLoadedOn(
        final Coordinate position,
        final String... names
    ) {
        return getLoadedOn(position, Items.getNamePredicate(names));
    }

    public static LocatableEntityQueryResults<GroundItem> getLoadedWithin(final Area area) {
        return getLoadedWithin(area, (Predicate<? super GroundItem>) null);
    }

    public static LocatableEntityQueryResults<GroundItem> getLoadedWithin(
        final Area area,
        final Predicate<? super GroundItem> filter
    ) {
        return OSRSGroundItems.getLoadedWithin(area, filter);
    }

    public static LocatableEntityQueryResults<GroundItem> getLoadedWithin(
        final Area area,
        final int... ids
    ) {
        return getLoadedWithin(area, Items.getIdPredicate(ids));
    }

    public static LocatableEntityQueryResults<GroundItem> getLoadedWithin(
        final Area area,
        final String... names
    ) {
        return getLoadedWithin(area, Items.getNamePredicate(names));
    }

    /**
     * Checks if any items match the given {@link Predicate filter}
     *
     * @param filter the predicate to check the items against
     * @return true if an item matches the filter
     */
    public static boolean contains(Predicate<GroundItem> filter) {
        return Items.contains(getLoaded(), filter);
    }

    /**
     * Checks if any items match the given id
     *
     * @param id the id to check the items against
     * @return true if an item matches the id
     */
    public static boolean contains(int id) {
        return Items.contains(getLoaded(), id);
    }

    /**
     * Checks if any items match the given name
     *
     * @param name the name to check the items against
     * @return true if an item matches the name
     */
    public static boolean contains(String name) {
        return Items.contains(getLoaded(), name);
    }

    /**
     * Checks if any items match the given name
     *
     * @param name the name to check the items against
     * @return true if an item matches the name
     */
    public static boolean contains(Pattern name) {
        return Items.contains(getLoaded(), name);
    }

    /**
     * Checks if the supplied {@link Predicate filter} matches at least one item
     *
     * @param predicate the predicate to check the items against
     * @return true if the predicate matches an item
     */
    public static boolean containsAllOf(final Predicate<GroundItem> predicate) {
        return Items.containsAllOf(getLoaded(), predicate);
    }

    /**
     * Checks if all of the supplied {@link Predicate filter}s match at least one item each
     *
     * @param filters the predicates to check the items against
     * @return true if all of the predicates have a match
     */
    @SafeVarargs
    public static boolean containsAllOf(final Predicate<GroundItem>... filters) {
        return Items.containsAllOf(getLoaded(), filters);
    }

    /**
     * Checks if all of the supplied ids match at least one item each
     *
     * @param ids the ids to check the items against
     * @return true if all of the ids have a match
     */
    public static boolean containsAllOf(final int... ids) {
        return Items.containsAllOf(getLoaded(), ids);
    }

    /**
     * Checks if all of the supplied names match at least one item each
     *
     * @param names the names to check the items against
     * @return true if all of the names have a match
     */
    public static boolean containsAllOf(final String... names) {
        return Items.containsAllOf(getLoaded(), names);
    }

    /**
     * Checks if all of the supplied names match at least one item each
     *
     * @param names the names to check the items against
     * @return true if all of the names have a match
     */
    public static boolean containsAllOf(final Pattern... names) {
        return Items.containsAllOf(getLoaded(), names);
    }

    /**
     * Checks if any items don't match the given {@link Predicate filter}
     *
     * @param filter the predicate to check the items against
     * @return true if at least one item doesn't match the filter
     */
    public static boolean containsAnyExcept(final Predicate<GroundItem> filter) {
        return Items.containsAnyExcept(getLoaded(), filter);
    }

    /**
     * Checks if any items don't match the given {@link Predicate filter}s
     *
     * @param filters the predicates to check the items against
     * @return true if at least one item doesn't match the filters
     */
    @SafeVarargs
    public static boolean containsAnyExcept(final Predicate<GroundItem>... filters) {
        return Items.containsAnyExcept(getLoaded(), filters);
    }

    /**
     * Checks if any items don't match the given ids
     *
     * @param ids the ids to check the items against
     * @return true if at least one item doesn't match the ids
     */
    public static boolean containsAnyExcept(final int... ids) {
        return Items.containsAnyExcept(getLoaded(), ids);
    }

    /**
     * Checks if any items don't match the given names
     *
     * @param names the names to check the items against
     * @return true if at least one item doesn't match the names
     */
    public static boolean containsAnyExcept(final String... names) {
        return Items.containsAnyExcept(getLoaded(), names);
    }

    /**
     * Checks if any items don't match the given names
     *
     * @param names the names to check the items against
     * @return true if at least one item doesn't match the names
     */
    public static boolean containsAnyExcept(final Pattern... names) {
        return Items.containsAnyExcept(getLoaded(), names);
    }

    /**
     * Checks if any item matches the given {@link Predicate filter}
     *
     * @param filter the filter to check the items against
     * @return true if at least one item matches the filter
     */
    public static boolean containsAnyOf(final Predicate<GroundItem> filter) {
        return Items.containsAnyOf(getLoaded(), filter);
    }

    /**
     * Checks if any item matches the given {@link Predicate filter}s
     *
     * @param filters the filters to check the items against
     * @return true if at least one item matches a filter
     */
    @SafeVarargs
    public static boolean containsAnyOf(final Predicate<GroundItem>... filters) {
        return Items.containsAnyOf(getLoaded(), filters);
    }

    /**
     * Checks if any item matches the given ids
     *
     * @param ids the ids to check the items against
     * @return true if at least one item matches an id
     */
    public static boolean containsAnyOf(final int... ids) {
        return Items.containsAnyOf(getLoaded(), ids);
    }

    /**
     * Checks if any item matches the given names
     *
     * @param names the names to check the items against
     * @return true if at least one item matches a name
     */
    public static boolean containsAnyOf(final String... names) {
        return Items.containsAnyOf(getLoaded(), names);
    }

    /**
     * Checks if any item matches the given names
     *
     * @param names the names to check the items against
     * @return true if at least one item matches a name
     */
    public static boolean containsAnyOf(final Pattern... names) {
        return Items.containsAnyOf(getLoaded(), names);
    }

    /**
     * Checks if all of the items match the given {@link Predicate filter}
     *
     * @param filter the filter to check the items against
     * @return true if all items match the filter
     */
    public static boolean containsOnly(final Predicate<GroundItem> filter) {
        return Items.containsOnly(getLoaded(), filter);
    }

    /**
     * Checks if all of the items match the given {@link Predicate filter}s
     *
     * @param filters the filters to check the items against
     * @return true if all items match at least one filter each
     */
    @SafeVarargs
    public static boolean containsOnly(final Predicate<GroundItem>... filters) {
        return Items.containsOnly(getLoaded(), filters);
    }

    /**
     * Checks if all of the items match the given names
     *
     * @param names the filters to check the items against
     * @return true if all items match at least one name each
     */
    public static boolean containsOnly(final String... names) {
        return Items.containsOnly(getLoaded(), names);
    }

    /**
     * Checks if all of the items match the given names
     *
     * @param names the filters to check the items against
     * @return true if all items match at least one name each
     */
    public static boolean containsOnly(final Pattern... names) {
        return Items.containsOnly(getLoaded(), names);
    }

    /**
     * Gets the total quantity of items
     *
     * @return the total quantity of items
     */
    public static int getQuantity() {
        return Items.getQuantity(getLoaded());
    }

    /**
     * Gets the total quantity of items matching the filter
     *
     * @param filter the filter to check the items against
     * @return the total quantity of items matching the filter
     */
    public static int getQuantity(final Predicate<GroundItem> filter) {
        return Items.getQuantity(getLoaded(), filter);
    }

    /**
     * Gets the total quantity of items matching the {@link Predicate filter}s
     *
     * @param filters the filters to check the items against
     * @return the total quantity of items matching the filters
     */
    @SafeVarargs
    public static int getQuantity(final Predicate<GroundItem>... filters) {
        return Items.getQuantity(getLoaded(), filters);
    }

    /**
     * Gets the total quantity of items matching the ids
     *
     * @param ids the ids to check the items against
     * @return the total quantity of items matching the ids
     */
    public static int getQuantity(final int... ids) {
        return Items.getQuantity(getLoaded(), ids);
    }

    /**
     * Gets the total quantity of items matching the names
     *
     * @param names the ids to check the items against
     * @return the total quantity of items matching the names
     */
    public static int getQuantity(final String... names) {
        return Items.getQuantity(getLoaded(), names);
    }

    /**
     * Gets the total quantity of items matching the names
     *
     * @param names the ids to check the items against
     * @return the total quantity of items matching the names
     */
    public static int getQuantity(final Pattern... names) {
        return Items.getQuantity(getLoaded(), names);
    }


    /**
     * @deprecated use {@link Items#getIdPredicate(int...)}
     */
    @Deprecated
    public static Predicate<GroundItem> getIdPredicate(final int... ids) {
        return Items.getIdPredicate(ids);
    }

    public static Predicate<GroundItem> getAreaPredicate(final Area... acceptedAreas) {
        return Locatables.getAreaPredicate(acceptedAreas);
    }

    /**
     * Gets a GroundItem filter that can be used to get a GroundItem with one of the specified names
     *
     * @param acceptedNames the names that are valid (case-sensitive)
     * @return a filter
     * @deprecated use {@link Items#getNamePredicate(String...)}
     */
    @Deprecated
    public static Predicate<GroundItem> getNamePredicate(final String... acceptedNames) {
        return Items.getNamePredicate(acceptedNames);
    }

    /**
     * Gets a GroundItem filter that can be used to get the items with the specified ground powers
     *
     * @param acceptedActions the powers that are valid (case-sensitive)
     * @return a filter
     * @deprecated use {@link Items#getGroundActionPredicate(String...)}
     */
    @Deprecated
    public static Predicate<GroundItem> getActionPredicate(final String... acceptedActions) {
        return Items.getGroundActionPredicate(acceptedActions);
    }
}
