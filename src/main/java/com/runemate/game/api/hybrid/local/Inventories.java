package com.runemate.game.api.hybrid.local;

import com.runemate.client.framework.open.*;
import com.runemate.client.game.account.open.*;
import com.runemate.client.game.open.*;
import com.runemate.game.api.hybrid.*;
import com.runemate.game.api.hybrid.cache.configs.*;
import com.runemate.game.api.hybrid.entities.*;
import com.runemate.game.api.hybrid.entities.definitions.*;
import com.runemate.game.api.hybrid.input.*;
import com.runemate.game.api.hybrid.local.hud.interfaces.*;
import com.runemate.game.api.hybrid.location.*;
import com.runemate.game.api.hybrid.queries.results.*;
import com.runemate.game.api.hybrid.region.*;
import com.runemate.game.api.hybrid.util.collections.*;
import com.runemate.game.internal.*;
import java.rmi.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.function.*;
import java.util.stream.*;
import lombok.*;
import lombok.extern.log4j.*;

@Log4j2
public final class Inventories {

    public static final Set<Long> UNDOCUMENTED_INVENTORIES = Collections.newSetFromMap(new ConcurrentHashMap<>());

    public static SpriteItemQueryResults lookup(final Documented location, final Predicate<SpriteItem> filter) {
        return lookup(location.getId(), filter);
    }

    public static SpriteItemQueryResults lookup(final Documented location) {
        return lookup(location, null);
    }

    public static SpriteItemQueryResults lookup(final long uid) {
        return lookup(uid, null);
    }


    @SneakyThrows(RemoteException.class)
    public static SpriteItemQueryResults lookup(final long uid, final Predicate<SpriteItem> filter) {
        final List<SpriteItem> items = new ArrayList<>(28);
        final List<Pair<Integer, Integer>> lookup = OpenInventory.lookup(uid);
        final Documented location = Documented.resolve(uid);
        if (!lookup.isEmpty()) {
            items.addAll(from(location, lookup));
            if (filter != null) {
                items.removeIf(filter.negate());
            }
        } else if (uid == Documented.BANK.getId() && Bank.isUsingCachedContents(Environment.getBot())) {
            return OpenAccountDetails.getTraversalProfile().getCachedBankItems().stream()
                .map(cached -> new SpriteItem(cached.getId(), cached.getQuantity(), cached.getIndex(), SpriteItem.Origin.BANK))
                .collect(Collectors.toCollection(SpriteItemQueryResults::new));
        }
        SpriteItemQueryResults results = new SpriteItemQueryResults(items);
        if (uid == Documented.BANK.getId() && !results.isEmpty()) {
            OpenAccountDetails.getTraversalProfile()
                .setCachedBankItems(results.stream()
                    .map(item -> new CachedBankItem(item.getId(), item.getQuantity(), item.getIndex()))
                    .collect(Collectors.toList()));
        }
        return results;
    }

    public static Map<String, List<SpriteItem>> lookupAll() {
        try {
            final HashMap<String, List<SpriteItem>> res = new HashMap<>(8);
            final Map<Long, List<Pair<Integer, Integer>>> results = OpenInventory.lookup();
            results.forEach((inventoryId, inventoryContents) -> {
                final Documented location = Documented.resolve(inventoryId);
                final String locationString = (location == null ? "UNMAPPED" : location.name()) + "{" + "uid=" + inventoryId + '}';
                final List<SpriteItem> items = from(location, inventoryContents);
                res.put(locationString, items);
            });
            return res;
        } catch (RemoteException e) {
            log.warn("Failed to lookup all inventories", e);
            return null;
        }
    }

    private static SpriteItem from(final Documented location, final int index, final Pair<Integer, Integer> item) {
        if (location == null) {
            return new SpriteItem(item.getLeft(), item.getRight(), index, SpriteItem.Origin.UNKNOWN);
        }
        return new SpriteItem(item.getLeft(), item.getRight(), index, location.origin);
    }

    private static List<SpriteItem> from(final Documented location, final List<Pair<Integer, Integer>> items) {
        final List<SpriteItem> results = new ArrayList<>(items.size());
        for (int i = 0; i < items.size(); i++) {
            final Pair<Integer, Integer> item = items.get(i);
            if (item.getLeft() == -1) {
                continue;
            }
            results.add(from(location, i, item));
        }
        return results;
    }


    public static Map<String, List<SpriteItem>> lookupUndocumented() {
        final HashMap<String, List<SpriteItem>> res = new HashMap<>(8);
        final long[] known = Arrays.stream(Documented.values()).mapToLong(Documented::getId).toArray();
        final Map<Long, List<Pair<Integer, Integer>>> results = OpenInventory.lookupNot(known);
        results.forEach((uid, value) -> {
            final String key = "UNDOCUMENTED" + "{" + "uid=" + uid + '}';
            final List<SpriteItem> items = from(null, value);

            res.put(key, items);

            if (!UNDOCUMENTED_INVENTORIES.contains(uid)) {
                Player avatar = Players.getLocal();
                if (avatar != null) {
                    Coordinate position = avatar.getPosition();
                    if (position != null) {
                        Actor target = avatar.getTarget();
                        Actor targeted = Npcs.newQuery().targeting(avatar).results().nearest();
                        log.trace("An undocumented inventory was uncovered: {}", uid);
                        final String descpription = "Bot: "
                            + Environment.getBot().getMetaData().getName()
                            + ", User: "
                            + Environment.getForumName()
                            + ", World: "
                            + Worlds.getCurrent()
                            + ", Player: "
                            + avatar.getName()
                            + ", Position: "
                            + position.getX()
                            + ","
                            + position.getY()
                            + ","
                            + position.getPlane()
                            + ", "
                            + (target != null ? "Targeting: " + target.getName() + ", " : "")
                            + (targeted != null ? "Targeted: " + targeted.getName() + ", " : "")
                            + "Last Mouse Target: "
                            + Mouse.getPreviousTarget()
                            + ", Nearest[Npc: "
                            + Npcs.newQuery().results().nearest()
                            + ", Object: "
                            + GameObjects.newQuery().filter(o -> {
                            GameObjectDefinition def;
                            return o.getSpecializedTypeIndicator() != 22 && (def = o.getDefinition()) != null && def.isInteractable();
                        }).results().nearest()
                            + "], Contents: "
                            + Arrays.toString(items.toArray());

                        ClientAlarms.onClientError("An undocumented inventory was uncovered: " + uid, descpription);
                        UNDOCUMENTED_INVENTORIES.add(uid);
                    }
                }
            }
        });
        return res;
    }

    public static boolean opened(final long inventoryId) {
        return OpenInventory.isOpen(inventoryId);
    }

    public static boolean opened(final long... inventoryIds) {
        return OpenInventory.isOpen(inventoryIds);
    }

    public static boolean opened(final Documented... inventories) {
        long[] ids = Arrays.stream(inventories).mapToLong(Documented::getId).toArray();
        return opened(ids);
    }

    public static boolean opened(final SpriteItem.Origin origin) {
        return opened(Arrays.stream(Documented.values()).filter(d -> d.origin == origin).mapToLong(Documented::getId).toArray());
    }

    public static Documented getOpened(final SpriteItem.Origin origin) {
        return Arrays.stream(Documented.values()).filter(d -> d.origin == origin && opened(d)).findAny().orElse(null);
    }

    @InternalAPI
    @RequiredArgsConstructor
    public enum Documented {
        FISHING_TRAWLER_REWARD(0, SpriteItem.Origin.REWARDS_CHEST),
        TRADE(90, SpriteItem.Origin.OUTGOING_TRADE_OFFER),
        TRADEOTHER(90 | 0x8000, SpriteItem.Origin.INCOMING_TRADE_OFFER),
        INVENTORY(93, SpriteItem.Origin.INVENTORY),
        EQUIPMENT(94, SpriteItem.Origin.EQUIPMENT),
        BANK(95, SpriteItem.Origin.BANK),
        PUZZLE_BOX(140, SpriteItem.Origin.SHOP),
        BARROWS_REWARD(141, SpriteItem.Origin.REWARDS_CHEST), //unsupported
        MONKEY_MADNESS_PUZZLE_BOX(221, SpriteItem.Origin.SHOP),
        DRIFT_NET_FISHING_REWARD(307, SpriteItem.Origin.REWARDS_CHEST), //unsupported
        LOOTING_BAG(516, SpriteItem.Origin.LOOTING_BAG),
        KINGDOM_OF_MISCELLANIA(390, SpriteItem.Origin.REWARDS_CHEST), //unsupported
        CHAMBERS_OF_XERIC_CHEST(581, SpriteItem.Origin.REWARDS_CHEST), //unsupported
        THEATRE_OF_BLOOD_CHEST(612, SpriteItem.Origin.REWARDS_CHEST), //unsupported
        SEED_VAULT(626, SpriteItem.Origin.SEED_VAULT), //unsupported
        GROUP_STORAGE(659, SpriteItem.Origin.BANK), //Bank screen, unsupported
        GROUP_STORAGE_INV(660, SpriteItem.Origin.INVENTORY), //Player inventory
        WILDERNESS_LOOT_CHEST(797, SpriteItem.Origin.REWARDS_CHEST), //unsupported
        TOMBS_OF_AMASCUT_SHOP_LIFE(807, SpriteItem.Origin.TOA_RAID_SHOP), //unsupported
        TOMBS_OF_AMASCUT_SHOP_CHAOS(808, SpriteItem.Origin.TOA_RAID_SHOP), //unsupported
        TOMBS_OF_AMASCUT_SHOP_POWER(809, SpriteItem.Origin.TOA_RAID_SHOP), //unsupported
        TOMBS_OF_AMASCUT_RAID_SUPPLIES(810, SpriteItem.Origin.TOA_RAID_SUPPLIES), //unsupported
        TOMBS_OF_AMASCUT_CHEST(811, SpriteItem.Origin.REWARDS_CHEST), //unsupported
        ;

        //Unused/unconfirmed
//        BOBS_BRILLIANT_AXES(1, SpriteItem.Origin.SHOP),
//        HORVIKS_ARMOUR_SHOP(2, SpriteItem.Origin.SHOP),
//        LUMBRIDGE_GENERAL_STORE(3, SpriteItem.Origin.SHOP),
//        SHOP(4, SpriteItem.Origin.SHOP),
//        AUBURYS_RUNE_SHOP(5, SpriteItem.Origin.SHOP),
//        LOWES_ARCHERY_EMPORIUM(7, SpriteItem.Origin.SHOP),
//        ZEKES_SUPERIOR_SCIMITARS(11, SpriteItem.Origin.SHOP),
//        LOUIES_ARMOURED_LEGS_BAZAAR(12, SpriteItem.Origin.SHOP),
//        AL_KHARID_GENERAL_STORE(13, SpriteItem.Origin.SHOP),
//        BETTYS_MAGIC_EMPORIUM(25, SpriteItem.Origin.SHOP),
//        GRUMS_GOLD_EXCHANGE(28, SpriteItem.Origin.SHOP),
//        GERRANTS_FISHY_BUSINESS(30, SpriteItem.Origin.SHOP),
//        DOMMIKS_CRAFTING_STORE(32, SpriteItem.Origin.SHOP),
//        GAIUSS_TWO_HANDED_SHOP(39, SpriteItem.Origin.SHOP),
//        ZAFFS_SUPERIOR_STAFFS(51, SpriteItem.Origin.SHOP),
//        WYDINS_FOOD_STORE(76, SpriteItem.Origin.SHOP),
//        SHANTAY_PASS_SHOP(78, SpriteItem.Origin.SHOP),
//        LEGENDS_GUILD_SHOP_OF_USEFUL_ITEMS(84, SpriteItem.Origin.SHOP),
//        OUTGOING_PRICE_CHECK(90, SpriteItem.Origin.OUTGOING_TRADE_OFFER),
//        BACKPACK(93, SpriteItem.Origin.INVENTORY),
//        DOM_ONIONS_REWARD_UPGRADES_SHOP(108, SpriteItem.Origin.SHOP),
//        THE_PICK_AND_LUTE(149, SpriteItem.Origin.SHOP),
//        PROSPECTOR_PERCYS_NUGGET_SHOP(187, SpriteItem.Origin.SHOP),
//        SLAYER_EQUIPMENT_SHOP(231, SpriteItem.Origin.SHOP),
//        CAPE_MERCHANT_IAN(237, SpriteItem.Origin.SHOP),
//        DRAYNOR_SEED_MARKET(273, SpriteItem.Origin.SHOP),
//        MAGE_OF_ZAMORAK_RUNE_SHOP(277, SpriteItem.Origin.SHOP),
//        GRACES_GRACEFUL_CLOTHING(321, SpriteItem.Origin.SHOP),
//        FORTUNATOS_FINE_WINE(384, SpriteItem.Origin.SHOP),
//        PEST_CONTROL_SQUIRE_SHOP(389, SpriteItem.Origin.SHOP),
//        WARRIORS_GUILD_FOOD_SHOP(410, SpriteItem.Origin.SHOP),
//        WARRIORS_GUILD_ARMOURY(411, SpriteItem.Origin.SHOP),
//        WARRIORS_GUILD_POTION_SHOP(412, SpriteItem.Origin.SHOP),
//        DOM_ONIONS_REWARD_RESOURCES_SHOP(464, SpriteItem.Origin.SHOP),
//        GRAND_EXCHANGE_COLLECT_SLOT_1(517, SpriteItem.Origin.GRAND_EXCHANGE),
//        GRAND_EXCHANGE_COLLECT_SLOT_2(518, SpriteItem.Origin.GRAND_EXCHANGE),
//        GRAND_EXCHANGE_COLLECT_SLOT_3(519, SpriteItem.Origin.GRAND_EXCHANGE),
//        GRAND_EXCHANGE_COLLECT_SLOT_4(520, SpriteItem.Origin.GRAND_EXCHANGE),
//        GRAND_EXCHANGE_COLLECT_SLOT_5(521, SpriteItem.Origin.GRAND_EXCHANGE),
//        GRAND_EXCHANGE_COLLECT_SLOT_6(522, SpriteItem.Origin.GRAND_EXCHANGE),
//        BEAST_OF_BURDEN(530, SpriteItem.Origin.BEAST_OF_BURDEN),
//        PET_SHOP(531, SpriteItem.Origin.SHOP),
//        BEEFY_BILLS_SUPPLIES(536, SpriteItem.Origin.SHOP),
//        SLAYER_EQUIPMENT_CANIFIS(538, SpriteItem.Origin.SHOP),
//        FREDAS_BOOTS(569, SpriteItem.Origin.SHOP),
//        HENDORS_AWESOME_ORES(587, SpriteItem.Origin.SHOP),
//        YARSULS_PRODIGIOUS_PICKAXES(588, SpriteItem.Origin.SHOP),
//        BETTAMAXS_SHOP(604, SpriteItem.Origin.SHOP),
//        DIANGOS_TOY_STORE(609, SpriteItem.Origin.SHOP),
//        MONEY_POUCH(623, SpriteItem.Origin.MONEY_POUCH),
//        MAGESTIXS_SUMMONING_SHOP(628, SpriteItem.Origin.SHOP),
//        SLAYER_EQUIPMENT_TAVERLEY(633, SpriteItem.Origin.SHOP),
//        GNOME_SHOPKEEPERS_ARMOURY(655, SpriteItem.Origin.SHOP),
//        COSMETIC_PLAYER_APPEARANCE(670, SpriteItem.Origin.PLAYER_APPEARANCE),
//        PLAYERS_WARDROBE(671, SpriteItem.Origin.PLAYER_APPEARANCE),
//        DEATH_ITEM_RECLAIMING(676, SpriteItem.Origin.UNKNOWN),
//        LOOT_INVENTORY(773, SpriteItem.Origin.LOOT_INVENTORY);

        private final long uid;
        private final SpriteItem.Origin origin;

        public static Documented resolve(final long uid) {
            for (final Documented location : values()) {
                if (location.getId() == uid) {
                    return location;
                }
            }
            return null;
        }

        public static SpriteItem.Origin getOrigin(long uid) {
            for (Documented documented : values()) {
                if (documented.uid == uid) {
                    return documented.getOrigin();
                }
            }
            return SpriteItem.Origin.UNKNOWN;
        }

        public SpriteItem.Origin getOrigin() {
            return origin;
        }

        public InventoryDefinition getDefinition() {
            return InventoryDefinitions.load((int) getId());
        }

        public long getId() {
            return uid;
        }

        @Override
        public String toString() {
            return "Inventory(name=" + name() + ", uid=" + uid + ")";
        }
    }
}
