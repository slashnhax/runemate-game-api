package com.runemate.game.api.script.framework.listeners.events;

import com.runemate.game.api.hybrid.local.*;
import lombok.*;

@Value
public class VarbitEvent implements Event {
    Varbit varbit;
    int oldValue;
    int newValue;

    public int getChange() {
        return newValue - oldValue;
    }
}
