package com.runemate.game.api.hybrid.entities.status;

import com.runemate.game.api.hybrid.*;
import com.runemate.game.api.hybrid.util.*;
import lombok.*;

@Value
public class Hitsplat implements Validatable {

    int id, damage;
    int endCycle;
    int secondaryId;
    int secondaryDamage;

    @Deprecated
    public int geId() {
        return id;
    }

    @Deprecated
    public int getStartCycle() {
        return secondaryDamage;
    }

    public Classification getClassification() {
        for (var classification : Classification.values()) {
            for (int classifier : classification.getClassifiers()) {
                if (classifier == id) {
                    return classification;
                }
            }
        }
        return Classification.UNCLASSIFIED;
    }

    @Override
    public boolean isValid() {
        return endCycle > RuneScape.getCurrentCycle();
    }

    public enum Classification {
        UNCLASSIFIED(),
        MISS(0, new int[] { 0, 141 }),

        //OSRS exclusives
        POISON(2, 142),
        DISEASE(4, -1),
        VENOM(5, -1),
        HEALING(6, 143),

        //Events triggered by other entities
        BLOCK_OTHER(13, false),
        DAMAGE_OTHER(17, false),
        DAMAGE_OTHER_CYAN(19, false),
        DAMAGE_OTHER_ORANGE(21, false),
        DAMAGE_OTHER_YELLOW(23, false),
        DAMAGE_OTHER_WHITE(25, false),

        //Events triggered by (or against) the local player
        BLOCK_ME(12, true),
        DAMAGE_ME(16, true),
        DAMAGE_ME_CYAN(18, true),
        DAMAGE_ME_ORANGE(20, true),
        DAMAGE_ME_YELLOW(22, true),
        DAMAGE_ME_WHITE(24, true),
        DAMAGE_MAX_ME(43, true),
        DAMAGE_MAX_ME_CYAN(44, true),
        DAMAGE_MAX_ME_ORANGE(45, true),
        DAMAGE_MAX_ME_YELLOW(46, true),
        DAMAGE_MAX_ME_WHITE(47, true),


        //Changed many revisions ago
        @Deprecated COMBAT(1, -1),

        //RS3 exclusives
        /**
         * Exclusive to damaging the bulwark beast
         */
        //ARMOUR_PENETRATING(-1, -2),
        @Deprecated MELEE_AUTO_ATTACK(-1, 132),
        @Deprecated MELEE_ABILITY(-1, 133),
        @Deprecated MELEE_CRITICAL_HIT(-1, 134),
        @Deprecated RANGED_AUTO_ATTACK(-1, 135),
        @Deprecated RANGED_ABILITY(-1, 136),
        @Deprecated RANGED_CRITICAL_HIT(-1, 137),
        @Deprecated MAGIC_AUTO_ATTACK(-1, 138),
        @Deprecated MAGIC_ABILITY(-1, 139),
        @Deprecated MAGIC_CRITICAL_HIT(-1, 140),
        /**
         * Generic damage caused by desert heat, freezing cold, failing agility obstacles, many boss special attacks.
         */
        @Deprecated TYPELESS(-1, 144),
        //DEFLECTION(-1, -2),
        //CANNON(-1, -2),
        /**
         * Received by Solak and dealt by the Eldritch crossbow
         */
        //BLIGHT(-1, 232),
        //INSTANT_KILL(-1, -2),
        /**
         * When at least 32,768 life points are healed, which is possible on bosses with at least 655,360 maximum life points.
         */
        //UBER_HEALING(-1, -2)
        @Deprecated BIG_GAME_HUNTER_BLUE(233),
        @Deprecated BIG_GAME_HUNTER_YELLOW(235),
        @Deprecated BIG_GAME_HUNTER_RED(250);

        private final int[] rs3_classifiers;

        private @Getter final int[] classifiers;
        private @Getter final boolean players;

        Classification() {
            this(-1);
        }

        Classification(int classifier, boolean players) {
            this.classifiers = new int[] { classifier };
            this.rs3_classifiers = new int[0];
            this.players = players;
        }

        Classification(int classifier) {
            this(classifier, classifier);
        }

        Classification(int osrs_classifier, int rs3_classifier) {
            this(new int[] { osrs_classifier }, new int[] { rs3_classifier });
        }

        Classification(int[] classifiers) {
            this(classifiers, classifiers);
        }

        Classification(int[] classifiers, int rs3_classifier) {
            this(classifiers, new int[] { rs3_classifier });
        }

        Classification(int osrs_classifier, int[] rs3_classifiers) {
            this(new int[] { osrs_classifier }, rs3_classifiers);
        }

        Classification(int[] classifiers, int[] rs3_classifiers) {
            this.classifiers = classifiers;
            this.rs3_classifiers = rs3_classifiers;
            this.players = false;
        }
    }
}
