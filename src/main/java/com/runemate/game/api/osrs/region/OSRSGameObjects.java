package com.runemate.game.api.osrs.region;

import com.runemate.client.game.open.*;
import com.runemate.commons.internal.scene.locations.*;
import com.runemate.game.api.hybrid.*;
import com.runemate.game.api.hybrid.entities.*;
import com.runemate.game.api.hybrid.location.*;
import com.runemate.game.api.hybrid.queries.results.*;
import com.runemate.game.api.hybrid.util.*;
import com.runemate.game.api.osrs.entities.*;
import java.io.*;
import java.util.*;
import java.util.function.*;
import java.util.stream.*;

public final class OSRSGameObjects {
    private static final LocatableEntityQueryResults<GameObject> EMPTY =
        new LocatableEntityQueryResults<>(Collections.emptyList(), null);

    private OSRSGameObjects() {
    }


    public static LocatableEntityQueryResults<GameObject> getLoaded(Predicate<GameObject> filter) {
        Object[] objectData = (Object[]) OpenRegion.getObjectsLoaded();
        Set<GameObject> objects;
        if (filter != null) {
            objects = convertToGameObjects(objectData, Scene.getBase()).stream().filter(filter).collect(Collectors.toSet());
        } else {
            objects = new HashSet<>(convertToGameObjects(objectData, Scene.getBase()));
        }
        return new LocatableEntityQueryResults<>(objects);
    }


    public static LocatableEntityQueryResults<GameObject> getLoadedOn(Coordinate c, Predicate<GameObject> filter) {
        Coordinate base = Scene.getBase();
        Object[] objectData = (Object[]) OpenRegion.getObjectsLoadedOn(
            new Serializable[] {
                new TileArea(new Tile(c.getX() - base.getX(), c.getY() - base.getY(), c.getPlane()))
            });
        Set<GameObject> objects;
        if (filter != null) {
            objects = convertToGameObjects(objectData, Scene.getBase()).stream().filter(filter).collect(Collectors.toSet());
        } else {
            objects = new HashSet<>(convertToGameObjects(objectData, base));
        }
        return new LocatableEntityQueryResults<>(objects);
    }


    public static LocatableEntityQueryResults<GameObject> getLoadedWithin(Area area, Predicate<GameObject> filter) {
        Area.Rectangular rectangular = area.toRectangular();
        Coordinate bl = rectangular.getBottomLeft();
        Coordinate tr = rectangular.getTopRight();
        Coordinate base = Scene.getBase();
        Object[] objectData = (Object[]) OpenRegion.getObjectsLoadedOn(new Serializable[] {
            new TileArea(
                new Tile(bl.getX() - base.getX(), bl.getY() - base.getY(), bl.getPlane()),
                new Tile(tr.getX() - base.getX(), tr.getY() - base.getY(), tr.getPlane())
            )
        });
        Set<GameObject> objects;
        if (filter != null) {
            objects = convertToGameObjects(objectData, Scene.getBase()).stream().filter(filter).collect(Collectors.toSet());
        } else {
            objects = new HashSet<>(convertToGameObjects(objectData, base));
        }
        return new LocatableEntityQueryResults<>(objects);
    }

    private static List<GameObject> convertToGameObjects(Object[] response, Coordinate regionBase) {
        List<GameObject> objects = new ArrayList<>(response.length / 4);
        for (int index = 0; index < response.length; index += 4) {
            long objectUid = (long) response[index];
            String objectType = (String) response[index + 1];
            int objectId = (int) response[index + 2];
            Coordinate objectPosition = new Coordinate((int) response[index + 3]).derive(regionBase.getX(), regionBase.getY());
            objects.add(new OSRSGameObject(objectUid, objectType, objectId, objectPosition));
        }
        return objects;
    }
}
