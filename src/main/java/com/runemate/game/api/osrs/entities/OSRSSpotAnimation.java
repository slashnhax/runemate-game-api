package com.runemate.game.api.osrs.entities;

import com.runemate.client.game.open.*;
import com.runemate.game.api.hybrid.*;
import com.runemate.game.api.hybrid.cache.configs.*;
import com.runemate.game.api.hybrid.cache.elements.*;
import com.runemate.game.api.hybrid.entities.*;
import com.runemate.game.api.hybrid.local.hud.*;
import com.runemate.game.api.hybrid.location.*;
import java.util.*;

public class OSRSSpotAnimation extends OSRSCacheModelEntity implements SpotAnimation {

    private OpenSpotAnimation spotAnimation;

    public OSRSSpotAnimation(long uid) {
        super(uid);
    }

    public OSRSSpotAnimation(OpenSpotAnimation spotAnimation) {
        super(spotAnimation.getUid());
        this.spotAnimation = spotAnimation;
    }

    private OpenSpotAnimation spotAnimation() {
        if (spotAnimation == null) {
            spotAnimation = OpenSpotAnimation.create(uid);
        }
        return spotAnimation;
    }

    @Override
    public int getId() {
        return spotAnimation().getId();
    }


    @Override
    public Coordinate getPosition(Coordinate regionBase) {
        if (regionBase == null) {
            regionBase = Scene.getBase();
        }
        int regionX = spotAnimation().getRegionX() >> 7;
        int regionY = spotAnimation().getRegionY() >> 7;
        int plane = spotAnimation().getPlane();
        return new Coordinate(
            regionBase.getX() + regionX,
            regionBase.getY() + regionY,
            plane
        );
    }

    @Override
    public Coordinate.HighPrecision getHighPrecisionPosition(Coordinate regionBase) {
        return getPosition(regionBase).getHighPrecisionPosition();
    }

    public SpotAnimationDefinition getDefinition() {
        return SpotAnimationDefinitions.load(getId());
    }

    @Override
    public int getStartCycle() {
        return spotAnimation().getStartCycle();
    }

    @Override
    public boolean isFinishedAnimating() {
        return spotAnimation().getFinishedAnimating();
    }

    @Override
    public Model getModel() {
        if (forcedModel != null) {
            return forcedModel;
        }
        if (cacheModel != null && cacheModel.isValid()) {
            return cacheModel;
        }
        SpotAnimationDefinition definition = getDefinition();
        if (definition != null) {
            CacheModel model = CacheModel.load(definition.getModelId());
            if (model != null) {
                return cacheModel = new CompositeCacheModel(this, Collections.singletonList(model));
            }
        }
        return backupModel;
    }

    @Override
    public String toString() {
        return "SpotAnimation[id=" + getId() + ", area=" + getArea() + "]";
    }


    @Override
    public int getAnimationId() {
        if (spotAnimation().getFinishedAnimating()) {
            return -1;
        }
        SpotAnimationDefinition definition = getDefinition();
        if (definition == null) {
            return -1;
        }
        return definition.getAnimationId();
    }
}
