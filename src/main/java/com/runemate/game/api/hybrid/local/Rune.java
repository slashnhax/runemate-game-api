package com.runemate.game.api.hybrid.local;

import com.runemate.game.api.hybrid.local.hud.interfaces.*;
import com.runemate.game.api.hybrid.util.*;
import com.runemate.game.api.osrs.local.*;
import java.util.*;
import java.util.function.*;
import java.util.regex.*;
import java.util.stream.*;
import lombok.*;

public enum Rune {
    AIR(1),
    WATER(2),
    EARTH(3),
    FIRE(4),
    MIND(5),
    CHAOS(6),
    DEATH(7),
    BLOOD(8),
    COSMIC(9),
    NATURE(10),
    LAW(11),
    BODY(12),
    SOUL(13),
    ASTRAL(14),
    MIST(15, AIR, WATER),
    MUD(16, EARTH, WATER),
    DUST(17, AIR, EARTH),
    LAVA(18, EARTH, FIRE),
    STEAM(19, WATER, FIRE),
    SMOKE(20, FIRE, AIR),
    WRATH(21);

    private static final String[] DEFAULT_STAFF_PATTERNS = {
        "Staff of {}",
        "{} battlestaff",
        "Mystic {} staff"
    };

    private static final Pattern TOME_OF_FIRE = Regex.getPatternForExactString("Tome of fire");
    private static final Pattern TOME_OF_WATER = Regex.getPatternForExactString("Tome of water");
    private static final Pattern KODAI_WAND = Regex.getPatternForExactString("Kodai wand");
    private static final Pattern RUNE_POUCH = Pattern.compile("(Divine )?rune pouch( \\(l\\))?", Pattern.CASE_INSENSITIVE);

    public final int pouchType;
    private final String name;
    private final Collection<Rune> replaces;

    @Getter(value = AccessLevel.PRIVATE, lazy = true)
    private final Predicate<SpriteItem> infiniteSourcePredicate = buildEquipmentPredicate();

    Rune(int pouchType, Rune... replaces) {
        this.pouchType = pouchType;
        this.name = name().charAt(0) + name().substring(1).toLowerCase() + " rune";
        this.replaces = Set.of(replaces);
    }

    public String getName() {
        return name;
    }

    /**
     * @return true if this Rune replaces other Runes.
     */
    public boolean isCombinationRune() {
        return !replaces.isEmpty();
    }

    /**
     * Returns the quantity of the runes available from the players inventory, RunePouch, and equipment from staves.
     * <p>
     * This is a mildly expensive call.
     *
     * @param provider The List of SpriteItems to get the total quantity of
     * @return the number of runes available for use or Integer.MAX_VALUE if using an item providing unlimited runes.
     */
    public int getQuantity(List<SpriteItem> provider) {
        //Check if we have any infinite sources for our current rune type
        if (provider.stream().anyMatch(getInfiniteSourcePredicate())) {
            return Integer.MAX_VALUE;
        }

        var inventory = provider.stream()
            .filter(item -> item.getOrigin() == SpriteItem.Origin.INVENTORY)
            .collect(Collectors.toSet());

        //Base quantity of runes in inventory
        var count = Items.getQuantity(inventory, name);

        //Add any runes of this type in the rune pouch
        if (Items.contains(inventory, RUNE_POUCH)) {
            count += RunePouch.getQuantity(this);
        }

        //Check if we have any of the replacing runes
        for (var rune : getReplacingRunes()) {
            var quantity = rune.getQuantity(provider);

            //We can return early if we have a replacing rune with an infinite source
            if (quantity == Integer.MAX_VALUE) {
                return Integer.MAX_VALUE;
            }

            count += quantity;
        }

        return count;
    }

    public int getQuantity() {
        var items = new ArrayList<SpriteItem>();
        items.addAll(Inventory.getItems());
        items.addAll(Equipment.getItems());
        return getQuantity(items);
    }

    private Predicate<SpriteItem> buildEquipmentPredicate() {
        return item -> {
            if (item.getOrigin() != SpriteItem.Origin.EQUIPMENT) {
                return false;
            }

            var definition = item.getDefinition();
            if (definition == null) {
                return false;
            }

            var name = definition.getName();
            if ("null".equals(name)) {
                return false;
            }

            for (var template : DEFAULT_STAFF_PATTERNS) {
                if (name.equalsIgnoreCase(template.replace("{}", name()))) {
                    return true;
                }
            }

            if (this == FIRE) {
                return TOME_OF_FIRE.matcher(name).find();
            }

            if (this == WATER) {
                return TOME_OF_WATER.matcher(name).find() || KODAI_WAND.matcher(name).find();
            }

            return false;
        };
    }

    /**
     * @return the runes that the rune instance replaces, empty if not a combo rune. eg. if this is a MIST rune, return AIR and WATER
     */
    public Set<Rune> getReplacedRunes() {
        return replaces.isEmpty() ? EnumSet.noneOf(Rune.class) : EnumSet.copyOf(replaces);
    }

    /**
     * @return the runes that replace the rune instance, empty if a combo rune or has no replacements. eg. if this is a WATER rune, return MIST, MUD and STEAM
     */
    public List<Rune> getReplacingRunes() {
        if (isCombinationRune()) {
            return Collections.emptyList();
        }
        return Arrays.stream(values()).filter(rune -> rune.getReplacedRunes().contains(this)).collect(Collectors.toList());
    }
}