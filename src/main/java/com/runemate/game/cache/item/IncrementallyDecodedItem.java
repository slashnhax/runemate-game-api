package com.runemate.game.cache.item;

import com.runemate.game.cache.io.*;
import java.io.*;
import java.util.*;

public abstract class IncrementallyDecodedItem implements DecodedItem {
    protected abstract void decode(Js5InputStream stream, int opcode) throws IOException;

    @Override
    public void decode(Js5InputStream stream) throws IOException {
        int opcode;
        List<Integer> decoded = new ArrayList<>();
        try {
            while ((opcode = stream.readUnsignedByte()) != 0) {
                decoded.add(opcode);
                decode(stream, opcode);
            }
        } catch (IOException exception) {
            exception.printStackTrace();
            throw new DecodingException(decoded.toArray(new Integer[0]));
        }
    }
}
