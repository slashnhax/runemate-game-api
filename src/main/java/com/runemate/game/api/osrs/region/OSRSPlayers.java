package com.runemate.game.api.osrs.region;

import com.runemate.client.game.open.*;
import com.runemate.game.api.hybrid.entities.*;
import com.runemate.game.api.hybrid.queries.results.*;
import com.runemate.game.api.osrs.entities.*;
import java.util.function.*;
import java.util.stream.*;

public final class OSRSPlayers {

    private OSRSPlayers() {
    }


    public static OSRSPlayer getAt(int index) {
        OpenPlayer node = OpenPlayer.atIndex(index);
        return node != null ? new OSRSPlayer(node) : null;
    }


    public static LocatableEntityQueryResults<Player> getLoaded(Predicate<? super Player> predicate) {
        OpenWorldView world = OpenWorldView.getTopLevelWorldView();
        return new LocatableEntityQueryResults<>(world.getPlayers()
            .stream()
            .filter(x -> x.getUid() > 0)
            .map(OSRSPlayer::new)
            .filter(x -> predicate == null || predicate.test(x))
            .collect(Collectors.toList()));
    }


    public static OSRSPlayer getLocal() {
        long uid = OpenPlayer.getLocal();
        return uid != 0 ? new OSRSPlayer(uid) : null;
    }
}
