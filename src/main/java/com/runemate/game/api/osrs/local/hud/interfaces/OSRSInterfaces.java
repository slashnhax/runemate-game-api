package com.runemate.game.api.osrs.local.hud.interfaces;

import com.runemate.client.game.open.*;
import com.runemate.game.api.hybrid.*;
import com.runemate.game.api.hybrid.local.hud.*;
import com.runemate.game.api.hybrid.local.hud.interfaces.*;
import com.runemate.game.api.hybrid.util.*;
import com.runemate.rmi.*;
import java.awt.*;
import java.util.List;
import java.util.*;
import java.util.function.*;
import org.jetbrains.annotations.*;

public final class OSRSInterfaces {
    private OSRSInterfaces() {
    }

    @Deprecated
    public static List<InterfaceContainer> getLoaded(final Predicate<InterfaceContainer> predicate) {
        final long[] interface_uids = OpenInterfaceContainer.getLoaded();
        final ArrayList<InterfaceContainer> containers = new ArrayList<>(interface_uids.length);
        for (int index = 0; index < interface_uids.length; ++index) {
            final long interface_uid = interface_uids[index];
            if (interface_uid != 0) {
                containers.add(new InterfaceContainer(interface_uid, index));
            }
        }
        containers.trimToSize();
        if (predicate == null) {
            return containers;
        }
        return containers.stream().filter(predicate).toList();
    }

    public static InterfaceComponent getAt(int containerIndex, int componentIndex) {
        return getAt(containerIndex, componentIndex, -1);
    }

    @Nullable
    public static InterfaceComponent getAt(int containerIndex, int componentIndex, int subComponentIndex) {
        return Optional.ofNullable(OpenInterfaceComponentManager.getLoadedComponent(containerIndex, componentIndex, subComponentIndex))
            .map(OSRSInterfaceComponent::new)
            .orElse(null);
    }

    @Deprecated
    public static InterfaceContainer getAt(final int index) {
        final long internal = OpenInterfaceContainer.getAt(index);
        if (BridgeUtil.getFieldInstance(internal) == 0) {
            return null;
        }
        return new InterfaceContainer(internal, index);
    }

    public static InteractableRectangle getRootBounds(final int arrayIndex) {
        final Rectangle rect = OpenInterfaceContainer.getRootBounds(arrayIndex);
        if (rect != null) {
            return new InteractableRectangle(rect);
        }
        return null;
    }
}
