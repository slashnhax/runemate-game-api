package com.runemate.bot.test;

import com.runemate.game.api.script.framework.*;
import com.runemate.ui.*;
import com.runemate.ui.setting.annotation.open.*;
import javafx.scene.control.*;
import lombok.extern.log4j.*;

/**
 * This bot can be used to test new features that you're working on.Recommend that you use this to make it obvious to the reviewer
 * how exactly the code change was tested.
 * <p>
 * The "testJar" gradle task will produce a jar containing this bot which you can place in a bot directory.
 * <p>
 * The following gradle command will build this bot and then launch the client:
 * testJar launch --args="--dev"
 */
@Log4j2
public class FeatureTestBot extends LoopingBot {

    @SettingsProvider(updatable = true)
    ExampleSettings settings;

    @Override
    public void onStart(final String... arguments) {
//        DefaultUI.showModalOverlay(this, new Button("XYZ"));
    }

    @Override
    public void onLoop() {

    }
}
