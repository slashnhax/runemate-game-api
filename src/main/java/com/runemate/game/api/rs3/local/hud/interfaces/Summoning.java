package com.runemate.game.api.rs3.local.hud.interfaces;

import com.runemate.game.api.hybrid.entities.details.*;
import java.util.*;
import org.jetbrains.annotations.*;

/**
 * @see BeastOfBurden
 */
@Deprecated
public final class Summoning {

    private Summoning() {
    }

    /**
     * Gets the current amount of summoning points available to the local player
     */
    public static int getPoints() {
        return 0;
    }

    /**
     * Gets the maximum amount of summoning points available to the local player
     */
    public static int getMaximumPoints() {
        return 0;
    }

    /**
     * Gets the amount of special move points available
     */
    public static int getSpecialMovePoints() {
        return 0;
    }

    /**
     * Gets the remaining time for the currently summoned familiar (in minutes)
     */
    public static int getMinutesRemaining() {
        return 0;
    }

    public static Set<Familiar> getBeastOfBurdens() {
        return Collections.emptySet();
    }

    @Deprecated
    public enum FamiliarOption {
        FOLLOWER_DETAILS,
        SPECIAL_MOVE,
        ATTACK,
        CALL_FOLLOWER,
        DISMISS_FOLLOWER,
        TAKE_BOB,
        RENEW_FAMILIAR,
        INTERACT;

        FamiliarOption() {
            // no-op
        }

        /**
         * Gets the option that occurs on the left click of the necessary component.
         *
         * @return
         */
        @Nullable
        public static FamiliarOption getLeftClick() {
            return null;
        }

        /**
         * Selects the option from the necessary component.
         *
         * @return
         */
        public boolean select() {
            return false;
        }

        /**
         * Sets it as the option to occur on left click of the necessary component.
         *
         * @return
         */
        public boolean setAsLeftClick() {
            return false;
        }
    }

    @Deprecated
    public enum Familiar implements Onymous {
        SPIRIT_WOLF,
        DREADFOWL,
        SPIRIT_SPIDER,
        THORNY_SNAIL,
        GRANITE_CRAB,
        SPIRIT_MOSQUITO,
        DESERT_WYRM,
        SPIRIT_SCORPION,
        SPIRIT_TZ_KIH,
        ALBINO_RAT,
        SPIRIT_KALPHITE,
        COMPOST_MOUND,
        GIANT_CHINCHOMPA,
        VAMPYRE_BAT,
        HONEY_BADGER,
        BEAVER,
        VOID_RAVAGER,
        VOID_SPINNER,
        VOID_SHIFTER,
        VOID_TORCHER,
        BRONZE_MINOTAUR,
        BULL_ANT,
        MACAW,
        EVIL_TURNIP,
        SPIRIT_COCKATRICE,
        SPIRIT_GUTHATRICE,
        SPIRIT_SARATRICE,
        SPIRIT_ZAMATRICE,
        SPIRIT_PENGATRICE,
        SPIRIT_CORAXATRICE,
        SPIRIT_VULATRICE,
        IRON_MINOTAUR,
        PYRELORD,
        MAGPIE,
        BLOATED_LEECH,
        SPIRIT_TERRORBIRD,
        ABYSSAL_PARASITE,
        SPIRIT_JELLY,
        STEEL_MINOTAUR,
        IBIS,
        SPIRIT_GRAAHK,
        SPIRIT_KYATT,
        SPIRIT_LARUPIA,
        KARAMTHULHU_OVERLORD,
        SMOKE_DEVIL,
        ABYSSAL_LURKER,
        SPIRIT_COBRA,
        STRANGER_PLANT,
        MITHRIL_MINOTAUR,
        BARKER_TOAD,
        WAR_TORTOISE,
        BUNYIP,
        FRUIT_BAT,
        RAVENOUS_LOCUST,
        ARCTIC_BEAR,
        PHOENIX,
        OBSIDIAN_GOLEM,
        GRANITE_LOBSTER,
        PRAYING_MANTIS,
        ADAMANT_MINOTAUR,
        FORGE_REGENT,
        TALON_BEAST,
        GIANT_ENT,
        FIRE_TITAN,
        ICE_TITAN,
        MOSS_TITAN,
        HYDRA,
        SPIRIT_DAGANNOTH,
        LAVA_TITAN,
        SWAMP_TITAN,
        RUNE_MINOTAUR,
        UNICORN_STALLION,
        GEYSER_TITAN,
        WOLPERTINGER,
        ABYSSAL_TITAN,
        IRON_TITAN,
        PACK_YAK,
        STEEL_TITAN,
        NIGHTMARE_MUSPAH,
        LIGHT_CREATURE;

        Familiar() {
            // no-op
        }

        public Scroll getScroll() {
            return Scroll.HOWL;
        }

        public Pouch getPouch() {
            return Pouch.SPIRIT_WOLF;
        }

        public Charm getCharm() {
            return Charm.GOLD;
        }

        public int getRequiredLevel() {
            return 0;
        }

        public int getSummoningCost() {
            return 0;
        }

        public int getSpecialMoveCost() {
            return 0;
        }

        public int getInventorySize() {
            return 0;
        }

        public int getLifePoints() {
            return 0;
        }

        public int getCombatLevel() {
            return 0;
        }

        public int getMinutesAvailable() {
            return 0;
        }

        public boolean isBeastOfBurden() {
            return false;
        }

        @Override
        public String getName() {
            return name();
        }

        public List<Ingredient> getIngredients() {
            return Collections.emptyList();
        }

        @Override
        public String toString() {
            return name();
        }
    }

    @Deprecated
    public enum Charm {
        GOLD,
        GREEN,
        CRIMSON,
        BLUE,
        ELDER;

        Charm() {
            // no-op
        }

        public String getName() {
            return name();
        }

        @Override
        public String toString() {
            return name();
        }
    }

    @Deprecated
    public enum Scroll implements Onymous {
        HOWL,
        DREADFOWL_STRIKE,
        EGG_SPAWN,
        SLIME_SPRAY,
        STONY_SHELL,
        PESTER,
        ELECTRIC_LASH,
        VENOM_SHOT,
        FIREBALL_ASSAULT,
        CHEESE_FEAST,
        SANDSTORM,
        GENERATE_COMPOST,
        EXPLODE,
        VAMPYRE_TOUCH,
        INSANE_FEROCITY,
        MULTICHOP,
        CALL_TO_ARMS,
        BRONZE_BULL_RUSH,
        UNBURDEN,
        HERB_CALL,
        EVIL_FLAMES,
        PETRIFYING_GAZE,
        IRON_BULL_RUSH,
        IMMENSE_HEAT,
        THIEVING_FINGERS,
        BLOOD_DRAIN,
        TIRELESS_RUN,
        ABYSSAL_DRAIN,
        DISSOLVE,
        STEEL_BULL_RUSH,
        FISH_RAIN,
        GOAD,
        AMBUSH,
        RENDING,
        DOOM_SPHERE,
        DUST_CLOUD,
        ABYSSAL_STEALTH,
        OPHIDIAN_INCUBATION,
        POISONOUS_BLAST,
        MITH_BULL_RUSH,
        TOAD_BARK,
        TETSUDO_SCROLL,
        SWALLOW_WHOLE,
        FRUIT_FALL,
        FAMINE,
        ARCTIC_BLAST,
        RISE_FROM_THE_ASHES,
        VOLCANIC_STREAM,
        CRUSHING_CLAW,
        MANTIS_STRIKE,
        ADDY_BULL_RUSH,
        INFERNO,
        DEADLY_CLAW,
        ACORN_MISSILE,
        TITANS_CONSTITUTION,
        REGROWTH,
        SPIKE_SHOT,
        EBON_THUNDER,
        SWAMP_PLAGUE,
        RUNE_BULL_RUSH,
        HEALING_AURA,
        BOIL,
        MAGIC_FOCUS,
        ESSENCE_SHIPMENT,
        IRON_WITHIN,
        WINTER_STORAGE,
        STEEL_OF_LEGENDS,
        SIPHON_SELF,
        ENLIGHTENMENT;

        Scroll() {
            // no-op
        }

        @Override
        public String getName() {
            return name();
        }

        @Override
        public String toString() {
            return name();
        }
    }

    @Deprecated
    public enum Ingredient implements Onymous {
        WOLF_BONES,
        RAW_CHICKEN,
        SPIDER_CARCASS,
        THIN_SNAIL,
        IRON_ORE,
        PROBOSCIS,
        BUCKET_OF_SAND,
        BRONZE_CLAW,
        OBSIDIAN_CHARM,
        RAW_RAT_MEAT,
        POTATO_CACTUS,
        COMPOST,
        CHINCHOMPA,
        VAMPYRE_DUST,
        HONEYCOMB,
        WILLOW_LOGS,
        RAVAGER_CHARM,
        SHIFTER_CHARM,
        SPINNER_CHARM,
        TORCHER_CHARM,
        BRONZE_BAR,
        MARIGOLDS,
        CLEAN_GUAM,
        CARVED_EVIL_TURNIP,
        COCKATRICE_EGG,
        GUTHATRICE_EGG,
        SARATRICE_EGG,
        ZAMATRICE_EGG,
        PENGATRICE_EGG,
        CORAXATRICE_EGG,
        VULATRICE_EGG,
        IRON_BAR,
        TINDERBOX,
        GOLD_RING,
        RAW_BEEF,
        RAW_BIRD_MEAT,
        ABYSSAL_CHARM,
        JUG_OF_WATER,
        STEEL_BAR,
        HARPOON,
        GRAAHK_FUR,
        KYATT_FUR,
        LARUPIA_FUR,
        FISH_BOWL,
        GOAT_HORN_DUST,
        SNAKE_HIDE,
        BAGGED_PLANT_1,
        MITHRIL_BAR,
        SWAMP_TOAD,
        TORTOISE_SHELL,
        RAW_SHARK,
        BANANA,
        POT_OF_FLOUR,
        POLAR_KEBBIT_FUR,
        PHOENIX_QUILL,
        GRANITE_500G,
        RED_FLOWERS,
        ADAMANT_BAR,
        RUBY_HARVEST,
        TALON_BEAST_CHARM,
        WILLOW_BRANCH,
        FIRE_TALISMAN,
        AIR_TALISMAN,
        WATER_TALISMAN,
        EARTH_TALISMAN,
        WATER_ORB,
        DAGANNOTH_HIDE,
        SWAMP_LIZARD,
        RUNE_BAR,
        UNICORN_HORN,
        RAW_RABBIT,
        IRON_PLATEBODY,
        YAK_HIDE,
        STEEL_PLATEBODY,
        MUSPAH_SPINE,
        LIGHT_CORE;

        Ingredient() {
            // no-op
        }

        @Override
        public String getName() {
            return name();
        }

        @Override
        public String toString() {
            return name();
        }
    }

    @Deprecated
    public enum Pouch implements Onymous {
        SPIRIT_WOLF,
        DREADFOWL,
        SPIRIT_SPIDER,
        THORNY_SNAIL,
        GRANITE_CRAB,
        SPIRIT_MOSQUITO,
        DESERT_WYRM,
        SPIRIT_SCORPION,
        SPIRIT_TZ_KIH,
        ALBINO_RAT,
        SPIRIT_KALPHITE,
        COMPOST_MOUND,
        GIANT_CHINCHOMPA,
        VAMPYRE_BAT,
        HONEY_BADGER,
        BEAVER,
        VOID_RAVAGER,
        VOID_SHIFTER,
        VOID_SPINNER,
        VOID_TORCHER,
        BRONZE_MINOTAUR,
        BULL_ANT,
        MACAW,
        EVIL_TURNIP,
        SPIRIT_SARATRICE,
        SPIRIT_PENGATRICE,
        SPIRIT_CORAXATRICE,
        SPIRIT_GUTHATRICE,
        SPIRIT_COCKATRICE,
        SPIRIT_ZAMATRICE,
        SPIRIT_VULATRICE,
        IRON_MINOTAUR,
        PYRELORD,
        MAGPIE,
        BLOATED_LEECH,
        SPIRIT_TERRORBIRD,
        ABYSSAL_PARASITE,
        SPIRIT_JELLY,
        IBIS,
        STEEL_MINOTAUR,
        SPIRIT_KYATT,
        SPIRIT_LARUPIA,
        SPIRIT_GRAAHK,
        KARAMTHULHU_OVERLORD,
        SMOKE_DEVIL,
        ABYSSAL_LURKER,
        SPIRIT_COBRA,
        STRANGER_PLANT,
        MITHRIL_MINOTAUR,
        BARKER_TOAD,
        WAR_TORTOISE,
        BUNYIP,
        FRUIT_BAT,
        RAVENOUS_LOCUST,
        ARCTIC_BEAR,
        PHOENIX,
        OBSIDIAN_GOLEM,
        GRANITE_LOBSTER,
        PRAYING_MANTIS,
        ADAMANT_MINOTAUR,
        FORGE_REGENT,
        TALON_BEAST,
        GIANT_ENT,
        MOSS_TITAN,
        ICE_TITAN,
        FIRE_TITAN,
        HYDRA,
        NIGHTMARE_MUSPAH,
        LAVA_TITAN,
        SPIRIT_DAGANNOTH,
        SWAMP_TITAN,
        RUNE_MINOTAUR,
        ICE_NIHIL,
        BLOOD_NIHIL,
        SHADOW_NIHIL,
        SMOKE_NIHIL,
        LIGHT_CREATURE,
        UNICORN_STALLION,
        GEYSER_TITAN,
        WOLPERTINGER,
        ABYSSAL_TITAN,
        IRON_TITAN,
        PACK_YAK,
        STEEL_TITAN;

        Pouch() {
            // no-op
        }

        /**
         * Gets the amount of spirit shards needed to create this pouch.
         *
         * @return an int
         */
        public int getSpiritShardsNeeded() {
            return 0;
        }

        public int getElderEnergyNeeded() {
            return 0;
        }

        @Override
        public String getName() {
            return name();
        }

        @Override
        public String toString() {
            return name();
        }
    }
}
