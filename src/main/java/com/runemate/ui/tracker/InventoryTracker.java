package com.runemate.ui.tracker;

import com.runemate.game.api.hybrid.entities.definitions.*;
import com.runemate.game.api.hybrid.local.hud.interfaces.*;
import com.runemate.game.api.hybrid.net.*;
import com.runemate.game.api.script.framework.*;
import com.runemate.game.api.script.framework.core.*;
import com.runemate.game.api.script.framework.listeners.*;
import com.runemate.game.api.script.framework.listeners.events.*;
import java.util.*;
import java.util.concurrent.*;
import javafx.application.*;
import javafx.beans.binding.*;
import javafx.beans.property.*;
import javafx.beans.value.*;
import javafx.collections.*;
import lombok.*;
import lombok.experimental.*;
import lombok.extern.log4j.*;
import org.jetbrains.annotations.*;

@Getter
@Log4j2
@FieldDefaults(level = AccessLevel.PRIVATE)
public class InventoryTracker implements InventoryListener, EquipmentListener, EngineListener {

    final AbstractBot bot;
    final ObservableMap<Integer, ItemTracker> items = FXCollections.observableMap(new ConcurrentHashMap<>());
    final IntegerProperty profit = new SimpleIntegerProperty(0);

    volatile boolean blocking = false;

    ItemEventFilter itemEventFilter = event -> {
        final var item = event.getItem();
        return item != null
            && !blocking
            && (item.getOrigin() == SpriteItem.Origin.INVENTORY
            || item.getOrigin() == SpriteItem.Origin.EQUIPMENT);
    };

    public InventoryTracker(final AbstractBot bot) {
        this.bot = bot;
        bot.getEventDispatcher().addListener(this);
    }

    @Nullable
    private synchronized ItemTracker getTracker(int id) {
        ItemDefinition def = ItemDefinition.get(id);
        if (def == null) {
            return null;
        }
        if (def.isNoted()) {
            id = def.getUnnotedId();
            def = ItemDefinition.get(id);
            if (def == null) {
                return null;
            }
        }
        ItemTracker item = items.get(id);
        if (item == null) {
            for (var entry : items.entrySet()) {
                if (Objects.equals(entry.getValue().getName(), def.getName())) {
                    item = entry.getValue();
                    items.put(id, item);
                    break;
                }
            }
        }
        if (item == null) {
            GrandExchange.Item lookup = def.isTradeable() ? GrandExchange.lookup(id) : null;
            item = new ItemTracker(id, def.getName(), lookup == null ? def.getShopValue() : lookup.getPrice());
            items.put(id, item);
        }
        return item;
    }

    @Override
    public synchronized void onItemEquipped(final ItemEvent event) {
        //Inverse of inventory operations, so we don't have -1 when we equip an item
        if (accept(event)) {
            processItemEvent(event);
        }
    }

    @Override
    public synchronized void onItemUnequipped(final ItemEvent event) {
        //Inverse of inventory operations, so we don't have +1 when we un-equip an item
        if (accept(event)) {
            processItemEvent(event);
        }
    }

    @Override
    public synchronized void onItemAdded(final ItemEvent event) {
        if (accept(event)) {
            processItemEvent(event);
        }
    }

    @Override
    public synchronized void onItemRemoved(final ItemEvent event) {
        if (accept(event)) {
            processItemEvent(event);
        }
    }

    public synchronized void processItemEvent(final ItemEvent event) {
        final int quantityChange = switch (event.getType()) {
            case ADDITION -> event.getQuantityChange();
            case REMOVAL -> -event.getQuantityChange();
            case UNKNOWN -> 0;
        };
        SpriteItem item = event.getItem();
        if (item != null) {
            update(item.getId(), quantityChange);
        }
    }

    public synchronized void update(final int itemId, final int quantityChange) {
        ItemTracker item = getTracker(itemId);
        if (item != null) {
            Platform.runLater(() -> {
                update(item.quantity, quantityChange);
                update(profit, item.value * quantityChange);
            });
        }
    }

    private synchronized void update(WritableNumberValue expr, int change) {
        if (expr == null) {
            return;
        }

        try {
            final var current = expr.getValue().intValue();
            expr.setValue(current + change);
        } catch (Exception e) {
            log.warn("Failed to update expression by {}", change, e);
        }
    }

    @Override
    public void onTickStart() {
        blocking = Bank.isOpen() || DepositBox.isOpen() || GrandExchange.isOpen();
    }

    public void setItemEventFilter(final ItemEventFilter filter) {
        this.itemEventFilter = filter;
    }

    public void setItemEventListening(boolean enabled) {
        final EventDispatcher dispatcher = bot.getEventDispatcher();
        final List<EventListener> listeners = dispatcher.getListeners();
        if (enabled && !listeners.contains(this)) {
            dispatcher.addListener(this);
        } else if (!enabled && listeners.contains(this)) {
            dispatcher.removeListener(this);
        }
    }

    private boolean accept(@NonNull ItemEvent event) {
        return itemEventFilter.test(event);
    }

    @Value
    public static class ItemTracker {

        int id;
        String name;
        int value;
        IntegerProperty quantity = new SimpleIntegerProperty(0);
        BooleanBinding showing = quantity.isNotEqualTo(0);
    }
}
