package com.runemate.game.api.rs3.local.hud.eoc;

import com.runemate.game.api.hybrid.local.hud.interfaces.*;
import com.runemate.game.api.hybrid.queries.results.*;
import java.awt.*;
import java.util.Collections;
import java.util.regex.*;

@Deprecated
public class ActionBarCommons {

    public static InterfaceComponentQueryResults getInteractionComponents() {
        return new InterfaceComponentQueryResults(Collections.emptyList());
    }

    public static InterfaceComponentQueryResults getKeyBindComponents() {
        return new InterfaceComponentQueryResults(Collections.emptyList());
    }

    public static InterfaceComponentQueryResults getCooldownIndicatorComponents() {
        return new InterfaceComponentQueryResults(Collections.emptyList());
    }
}
