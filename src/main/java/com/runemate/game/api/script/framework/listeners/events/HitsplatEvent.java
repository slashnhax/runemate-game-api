package com.runemate.game.api.script.framework.listeners.events;

import com.runemate.game.api.hybrid.entities.*;
import com.runemate.game.api.hybrid.entities.status.*;
import lombok.*;

@Value
public class HitsplatEvent implements EntityEvent {

    EntityType entityType;
    Actor source;
    Hitsplat hitsplat;

    /**
     * @deprecated use #getEntityType()
     */
    @Deprecated
    public Type getType() {
        return Type.valueOf(entityType.name());
    }

    @Deprecated
    public enum Type {
        NPC, PLAYER
    }
}
