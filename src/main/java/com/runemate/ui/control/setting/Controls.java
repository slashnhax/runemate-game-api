package com.runemate.ui.control.setting;

import com.runemate.game.api.hybrid.*;
import com.runemate.game.api.hybrid.location.*;
import com.runemate.game.api.hybrid.region.*;
import com.runemate.game.api.script.framework.*;
import com.runemate.game.api.script.framework.core.*;
import com.runemate.game.api.script.framework.listeners.events.*;
import com.runemate.game.internal.*;
import com.runemate.ui.*;
import com.runemate.ui.control.*;
import com.runemate.ui.converter.*;
import com.runemate.ui.setting.annotation.open.*;
import com.runemate.ui.setting.open.*;
import java.lang.reflect.*;
import java.time.Duration;
import java.util.*;
import java.util.concurrent.*;
import javafx.beans.binding.*;
import javafx.beans.property.*;
import javafx.beans.value.*;
import javafx.collections.*;
import javafx.scene.control.*;
import javafx.scene.input.*;
import javafx.util.*;
import lombok.*;
import lombok.extern.log4j.*;
import org.apache.commons.text.*;

@Log4j2(topic = "Settings")
@InternalAPI
public class Controls {

    Control build(AbstractBot bot, SettingsManager manager, Settings.Setting setting) {
        Type type = setting.setting().type();
        if (type == boolean.class) {
            return forBoolean(manager, setting);
        }

        if (type == int.class) {
            return forInteger(manager, setting);
        }

        if (type == double.class) {
            return forDouble(manager, setting);
        }

        if (type == String.class) {
            return forString(manager, setting);
        }

        if (type == EquipmentLoadout.class) {
            return forEquipmentLoadout(bot, manager, setting);
        }

        if (type == Coordinate.class) {
            return forCoordinate(bot.getPlatform(), manager, setting);
        }

        if (type == Duration.class) {
            return forDuration(manager, setting);
        }

        if (((Class<?>) type).isEnum()) {
            return forEnum(manager, setting);
        }

        CustomSettingControlProducer<?, ? extends Control> producer = getProducer(setting);
        if (producer != null) {
            Control control = producer.produce(bot, manager, setting.setting());
            if (control == null) {
                log.warn("Producer for {} was unable to build a suitable control", type);
                return null;
            }

            CustomSettingControl<?> custom = (CustomSettingControl<?>) control;
            SettingConverter converter = manager.getConverter(setting.setting());
            setInitialValue(custom.valueProperty(), manager, setting, converter);
            Bindings.bindBidirectional(setting.settingProperty(), custom.valueProperty(), mapper(setting, converter));
            custom.valueProperty().addListener(new SettingChangeListener<>(setting, converter));
            return defaults(manager, setting, control);
        }

        log.warn("No producer available for {}", setting.setting().key());
        return null;
    }

    private static CustomSettingControlProducer<?, ?> getProducer(Settings.Setting setting) {
        try {
            ControlProducer cp = setting.setting().getProperty(ControlProducer.class);
            if (cp == null) {
                log.warn("Unable to build setting control for {}, no control producer found!", setting.setting().key());
                return null;
            }

            return cp.value().getDeclaredConstructor().newInstance();
        } catch (ReflectiveOperationException e) {
            log.warn("Unable to create ControlProducer for {}", setting.setting().key(), e);
            return null;
        }
    }

    private static Control forBoolean(SettingsManager manager, Settings.Setting setting) {
        var cb = defaults(manager, setting, new CheckBox());
        var converter = manager.getConverter(setting.setting());
        setInitialValue(cb.selectedProperty(), manager, setting, converter);
        Bindings.bindBidirectional(setting.settingProperty(), cb.selectedProperty(), mapper(setting, converter));
        cb.selectedProperty().addListener(new SettingChangeListener<>(setting, converter));
        return cb;
    }

    private static Control forInteger(SettingsManager manager, Settings.Setting setting) {
        var spinner = defaults(manager, setting, new Spinner<Integer>());
        var factory = new SpinnerValueFactory.IntegerSpinnerValueFactory(0, Integer.MAX_VALUE);

        var range = setting.setting().getProperty(Range.class);
        if (range != null) {
            factory.setMin(range.min());
            factory.setMax(range.max());
            factory.setAmountToStepBy(range.step());
        }

        var suffix = setting.setting().getProperty(Suffix.class);
        if (suffix != null) {
            factory.setConverter(new SuffixConverter(suffix));
        }

        spinner.setEditable(true);
        spinner.setValueFactory(factory);

        var converter = manager.getConverter(setting.setting());
        setInitialValue(factory.valueProperty(), manager, setting, converter);
        Bindings.bindBidirectional(setting.settingProperty(), factory.valueProperty(), mapper(setting, converter));
        factory.valueProperty().addListener(new SettingChangeListener<>(setting, converter));
        return spinner;
    }

    private static Control forDouble(SettingsManager manager, Settings.Setting setting) {
        var spinner = defaults(manager, setting, new Spinner<Double>());
        var factory = new SpinnerValueFactory.DoubleSpinnerValueFactory(0, Double.MAX_VALUE);
        factory.setAmountToStepBy(0.1);
        spinner.setEditable(true);
        spinner.setValueFactory(factory);
        var converter = manager.getConverter(setting.setting());
        setInitialValue(factory.valueProperty(), manager, setting, converter);
        Bindings.bindBidirectional(setting.settingProperty(), factory.valueProperty(), mapper(setting, converter));
        factory.valueProperty().addListener(new SettingChangeListener<>(setting, converter));
        return spinner;
    }

    private static Control forString(SettingsManager manager, Settings.Setting setting) {
        TextInputControl control;
        if (setting.setting().setting().secret()) {
            control = new PasswordField();
        } else if (setting.setting().getProperty(Multiline.class) != null) {
            control = new TextArea();
            ((TextArea) control).prefRowCountProperty().bind(control.textProperty().orElse("-").map(text -> Math.min(6, Math.max(2, text.lines().count()))));
        } else {
            control = new TextField();
        }
        control.setPrefWidth(120);

        defaults(manager, setting, control);
        control.setPromptText(setting.setting().setting().description());
        var converter = manager.getConverter(setting.setting());
        setInitialValue(control.textProperty(), manager, setting, converter);
        Bindings.bindBidirectional(setting.settingProperty(), control.textProperty(), mapper(setting, converter));
        control.textProperty().addListener(new SettingChangeListener<>(setting, converter));
        return control;
    }

    @SuppressWarnings({ "unchecked", "rawtypes" })
    private static Control forEnum(SettingsManager manager, Settings.Setting setting) {
        var control = defaults(manager, setting, new ComboBox<Enum<?>>());
        var type = (Class<? extends Enum>) setting.setting().type();
        control.setMaxWidth(Double.MAX_VALUE);
        control.setItems(FXCollections.observableArrayList(type.getEnumConstants()));
        control.setConverter(new EnumConverter());

        var converter = manager.getConverter(setting.setting());
        setInitialValue(control.valueProperty(), manager, setting, converter);
        Bindings.bindBidirectional(setting.settingProperty(), control.valueProperty(), mapper(setting, converter));
        control.valueProperty().addListener(new SettingChangeListener<>(setting, converter));
        return control;
    }

    private static Control forEquipmentLoadout(AbstractBot bot, SettingsManager manager, Settings.Setting setting) {
        var button = defaults(manager, setting, new Button("Equipment"));
        var property = new SimpleObjectProperty<>(new EquipmentLoadout());
        property.addListener((obs, old, loadout) -> button.setText(loadout.isEmpty() ? "Not set" : loadout.size() + " items"));

        button.setOnAction(event -> {
            ModalEquipmentOverlay overlay = new ModalEquipmentOverlay(bot.getPlatform(), setting, property.get());
            EquipmentLoadout loadout = DefaultUI.showModalOverlay(bot, overlay.getOverlay());
            Optional.ofNullable(loadout).ifPresent(property::set);
        });

        var converter = manager.getConverter(setting.setting());
        setInitialValue(property, manager, setting, converter);
        Bindings.bindBidirectional(setting.settingProperty(), property, mapper(setting, converter));
        property.addListener(new SettingChangeListener<>(setting, converter));
        return button;
    }

    private static Control forCoordinate(BotPlatform platform, SettingsManager manager, Settings.Setting setting) {
        var button = defaults(manager, setting, new Button("Not set"));
        var property = new SimpleObjectProperty<Coordinate>(null);
        property.addListener((obs, old, coord) -> button.setText(coord == null ? "Not set" : coord.toString()));

        button.setOnMouseClicked(event -> {
            if (event.getButton() == MouseButton.SECONDARY) {
                property.set(null);
            }
        });

        button.setOnAction(event -> {
            button.setText("Loading...");
            try {
                var position = platform.invokeAndWait(() -> {
                    var player = Players.getLocal();
                    return player != null ? player.getPosition() : null;
                });
                property.set(position);
            } catch (ExecutionException | InterruptedException ignored) {
                log.warn("Failed to resolve player position for {}", setting.setting().key());
            }
        });

        var converter = manager.getConverter(setting.setting());
        setInitialValue(property, manager, setting, converter);
        Bindings.bindBidirectional(setting.settingProperty(), property, mapper(setting, converter));
        property.addListener(new SettingChangeListener<>(setting, converter));
        return button;
    }

    private static Control forDuration(SettingsManager manager, Settings.Setting setting) {
        var spinner = new DurationSpinner();
        var control = defaults(manager, setting, spinner);
        var factory = spinner.getValueFactory();

        var converter = manager.getConverter(setting.setting());
        setInitialValue(factory.valueProperty(), manager, setting, converter);
        Bindings.bindBidirectional(setting.settingProperty(), factory.valueProperty(), mapper(setting, converter));
        factory.valueProperty().addListener(new SettingChangeListener<>(setting, converter));
        return control;
    }

    @SuppressWarnings("unchecked")
    private static <T> void setInitialValue(Property<T> property, SettingsManager manager, Settings.Setting setting, SettingConverter converter) {
        try {
            T value = (T) converter.fromString(manager.get(setting.group(), setting.setting()), setting.setting().type());
            property.setValue(value);
        } catch (Exception e) {
            //suppressed
            e.printStackTrace();
        }
    }

    @RequiredArgsConstructor
    private static class SettingChangeListener<T> implements ChangeListener<T> {

        private final Settings.Setting setting;
        private final SettingConverter converter;

        @Override
        public void changed(final ObservableValue<? extends T> observable, final T oldValue, final T newValue) {
            setting.setSettingValue(converter.toString(newValue), SettingChangedEvent.Source.USER);
        }
    }

    private static <T> StringConverter<T> mapper(Settings.Setting setting, SettingConverter converter) {
        return new StringConverter<>() {
            @Override
            public String toString(final T object) {
                return converter.toString(object);
            }

            @Override
            @SuppressWarnings("unchecked")
            public T fromString(final String string) {
                try {
                    return (T) converter.fromString(string, setting.setting().type());
                } catch (Exception e) {
                    log.warn("Error deserializing {}.{}", setting.group().group().group(), setting.setting().key(), e);
                    return null;
                }
            }
        };
    }

    private static <T extends Control> T defaults(SettingsManager manager, Settings.Setting setting, T control) {
        control.setVisible(setting.shouldBeShown());
        control.setFocusTraversable(true);
        control.managedProperty().bind(control.visibleProperty());
        if (setting.setting().setting().disabled()) {
            control.setDisable(true);
        } else {
            control.disableProperty().bind(manager.lockedProperty());
        }

        return control;
    }

    private static class EnumConverter extends StringConverter<Enum<?>> {

        @Override
        public String toString(Enum<?> o) {
            if (o == null) {
                return null;
            }

            String toString = o.toString();
            //base toString() impl returns name()
            if (o.name().equals(toString)) {
                return WordUtils.capitalize(toString.toLowerCase(), '_').replace("_", " ");
            }

            return toString;
        }

        @Override
        public Enum<?> fromString(final String string) {
            return null;
        }
    }
}
