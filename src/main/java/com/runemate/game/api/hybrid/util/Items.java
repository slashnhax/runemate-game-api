package com.runemate.game.api.hybrid.util;

import com.runemate.game.api.hybrid.entities.*;
import com.runemate.game.api.hybrid.entities.definitions.*;
import java.util.*;
import java.util.function.*;
import java.util.regex.*;
import java.util.stream.*;

public class Items {

    /**
     * Item utility class
     */
    private Items() {
    }

    /**
     * Checks that the Item has at any inventory actions matching the given actions
     *
     * @param actions the actions to search
     * @param <T>     a type extending Item (e.g. SpriteItem, GroundItem)
     * @return true if the item has an inventory action matching any of the supplied actions
     */
    public static <T extends Item> Predicate<T> getInventoryActionPredicate(
        final String... actions
    ) {
        return item -> {
            ItemDefinition def = item.getDefinition();
            Collection<String> targets = Arrays.asList(actions);
            return def != null && def.getInventoryActions().stream().anyMatch(targets::contains);
        };
    }

    public static <T extends Item> Predicate<T> getInventoryActionPredicate(
        final Pattern... actions
    ) {
        return item -> {
            ItemDefinition def = item.getDefinition();
            return def != null && def.getInventoryActions().stream()
                .anyMatch(action -> Arrays.stream(actions)
                    .anyMatch(target -> target.matcher(action).find()));
        };
    }

    public static <T extends Item> Predicate<T> getGroundActionPredicate(final String... actions) {
        return item -> {
            ItemDefinition def = item.getDefinition();
            Collection<String> targets = Arrays.asList(actions);
            return def != null && def.getGroundActions().stream().anyMatch(targets::contains);
        };
    }

    public static <T extends Item> Predicate<T> getGroundActionPredicate(final Pattern... actions) {
        return item -> {
            ItemDefinition def = item.getDefinition();
            Collection<Pattern> targets = Arrays.asList(actions);
            return def != null && def.getGroundActions().stream()
                .anyMatch(
                    action -> targets.stream().anyMatch(target -> target.matcher(action).find()));
        };
    }

    public static <T extends Item> Predicate<T> getIdPredicate(final int... ids) {
        return item -> Arrays.stream(ids).anyMatch(id -> id == item.getId());
    }

    public static <T extends Item> Predicate<T> getNamePredicate(final String... names) {
        return item -> {
            ItemDefinition def = item.getDefinition();
            return def != null && Arrays.stream(names).anyMatch(name -> Objects.equals(name, def.getName()));
        };
    }

    public static <T extends Item> Predicate<T> getNamePredicate(final Pattern... names) {
        return item -> {
            ItemDefinition def = item.getDefinition();
            return def != null && Arrays.stream(names).anyMatch(name -> {
                String itemName = def.getName();
                return name != null && name.matcher(itemName).find();
            });
        };
    }

    public static <T extends Item> Predicate<T> getNamePredicate(final Collection<Pattern> names) {
        return item -> {
            ItemDefinition def = item.getDefinition();
            return def != null && names.stream().anyMatch(name -> {
                String itemName = def.getName();
                return name == null && itemName == null || name != null && name.matcher(itemName).find();
            });
        };
    }

    public static <T extends Item> Predicate<T> getQuantityPredicate(final int... quantities) {
        return item -> Arrays.stream(quantities)
            .anyMatch(quantity -> item.getQuantity() == quantity);
    }

    public static <T extends Item> boolean containsAnyOf(
        Collection<T> items,
        Predicate<T> predicate
    ) {
        return items.stream().anyMatch(predicate);
    }

    public static <T extends Item> boolean containsAnyOf(
        Collection<T> items,
        Predicate<T>... predicates
    ) {
        return Arrays.stream(predicates).anyMatch(item -> items.stream().anyMatch(item));
    }

    public static <T extends Item> boolean containsAnyOf(Collection<T> items, int... ids) {
        return containsAnyOf(items, getIdPredicate(ids));
    }

    public static <T extends Item> boolean containsAnyOf(Collection<T> items, String... names) {
        return containsAnyOf(items, getNamePredicate(names));
    }

    public static <T extends Item> boolean containsAnyOf(Collection<T> items, Pattern... names) {
        return containsAnyOf(items, getNamePredicate(names));
    }

    public static <T extends Item> boolean containsAnyOf(
        Collection<T> items,
        Collection<Pattern> names
    ) {
        return containsAnyOf(items, getNamePredicate(names));
    }

    public static <T extends Item> boolean contains(Collection<T> items, Predicate<T> predicate) {
        return containsAnyOf(items, predicate);
    }

    public static <T extends Item> boolean contains(Collection<T> items, int id) {
        return contains(items, getIdPredicate(id));
    }

    public static <T extends Item> boolean contains(Collection<T> items, String name) {
        return contains(items, getNamePredicate(name));
    }

    public static <T extends Item> boolean contains(Collection<T> items, Pattern name) {
        return contains(items, getNamePredicate(name));
    }

    public static <T extends Item> boolean containsAllOf(
        Collection<T> items,
        Predicate<T> predicate
    ) {
        return items.stream().allMatch(predicate);
    }

    public static <T extends Item> boolean containsAllOf(
        Collection<T> items,
        Predicate<T>... predicates
    ) {
        return Arrays.stream(predicates).allMatch(predicate -> items.stream().anyMatch(predicate));
    }

    public static <T extends Item> boolean containsAllOf(Collection<T> items, int... ids) {
        Set<Integer> itemIds =
            items.stream().mapToInt(T::getId).boxed().collect(Collectors.toSet());
        return Arrays.stream(ids).allMatch(itemIds::contains);
    }

    public static <T extends Item> boolean containsAllOf(Collection<T> items, String... names) {
        Set<String> nameSet = items.stream().map(item -> {
            ItemDefinition def = item.getDefinition();
            return def == null ? null : def.getName();
        }).filter(Objects::nonNull).collect(Collectors.toSet());
        return nameSet.containsAll(Arrays.asList(names));
    }

    public static <T extends Item> boolean containsAllOf(Collection<T> items, Pattern... names) {
        List<String> nameSet = items.stream().map(item -> {
            ItemDefinition def = item.getDefinition();
            return def == null ? null : def.getName();
        }).filter(Objects::nonNull).collect(Collectors.toList());
        return Arrays.stream(names)
            .allMatch(pattern -> nameSet.stream().anyMatch(pattern.asPredicate()));
    }

    public static <T extends Item> boolean containsOnly(
        Collection<T> items,
        Predicate<T> predicate
    ) {
        return items.stream().allMatch(predicate);
    }

    public static <T extends Item> boolean containsOnly(
        Collection<T> items,
        Predicate<T>... predicates
    ) {
        return items.stream()
            .allMatch(item -> Arrays.stream(predicates).anyMatch(pred -> pred.test(item)));
    }

    public static <T extends Item> boolean containsOnly(Collection<T> items, int... ids) {
        Set<Integer> suppliedIds = Arrays.stream(ids).boxed().collect(Collectors.toSet());
        return items.stream().allMatch(item -> suppliedIds.contains(item.getId()));
    }

    public static <T extends Item> boolean containsOnly(Collection<T> items, String... names) {
        return items.stream().allMatch(getNamePredicate(names));
    }

    public static <T extends Item> boolean containsOnly(Collection<T> items, Pattern... names) {
        return items.stream().allMatch(getNamePredicate(names));
    }

    public static <T extends Item> boolean containsAnyExcept(
        Collection<T> items,
        Predicate<T> predicate
    ) {
        return items.stream().anyMatch(predicate.negate());
    }

    public static <T extends Item> boolean containsAnyExcept(
        Collection<T> items,
        Predicate<T>... predicates
    ) {
        return items.stream().anyMatch(
            item -> Arrays.stream(predicates).noneMatch(predicate -> predicate.test(item)));
    }

    public static <T extends Item> boolean containsAnyExcept(Collection<T> items, int... ids) {
        return containsAnyExcept(items, getIdPredicate(ids));
    }

    public static <T extends Item> boolean containsAnyExcept(Collection<T> items, String... names) {
        return containsAnyExcept(items, getNamePredicate(names));
    }

    public static <T extends Item> boolean containsAnyExcept(
        Collection<T> items,
        Pattern... names
    ) {
        return containsAnyExcept(items, getNamePredicate(names));
    }

    public static <T extends Item> int getQuantity(Collection<T> items) {
        return items.stream().mapToInt(T::getQuantity).sum();
    }

    public static <T extends Item> int getQuantity(Collection<T> items, Predicate<T> predicate) {
        return items.stream().filter(predicate).mapToInt(T::getQuantity).sum();
    }

    public static <T extends Item> int getQuantity(Collection<T> items, Predicate<T>... filters) {
        return items.stream()
            .filter(item -> Arrays.stream(filters).anyMatch(filter -> filter.test(item)))
            .mapToInt(T::getQuantity).sum();
    }

    public static <T extends Item> int getQuantity(Collection<T> items, int... ids) {
        return getQuantity(items, getIdPredicate(ids));
    }

    public static <T extends Item> int getQuantity(Collection<T> items, String... names) {
        return getQuantity(items, getNamePredicate(names));
    }

    public static <T extends Item> int getQuantity(Collection<T> items, Pattern... names) {
        return getQuantity(items, getNamePredicate(names));
    }
}