package com.runemate.game.api.rs3.entities;

import com.runemate.game.api.hybrid.entities.*;
import com.runemate.game.api.hybrid.entities.definitions.*;
import com.runemate.game.api.hybrid.entities.status.*;
import com.runemate.game.api.hybrid.local.hud.*;
import com.runemate.game.api.hybrid.location.*;
import com.runemate.game.api.hybrid.util.collections.*;
import com.runemate.game.api.rs3.local.hud.interfaces.*;
import java.awt.*;
import java.util.List;
import java.util.regex.*;
import javafx.scene.canvas.*;
import lombok.*;
import org.jetbrains.annotations.*;

/**
 * A summoned npc with special characteristics
 */
@Deprecated
public final class SummonedFamiliar implements Npc {

    public SummonedFamiliar(final Summoning.Familiar info) {
        // no-op
    }

    /**
     * Gets the enum information about this familiar
     */
    public Summoning.Familiar getInfo() {
        return null;
    }

    @Override
    public String toString() {
        return null;
    }

    @Override
    public int getAnimationFrame() {
        return 0;
    }

    @Override
    public int getStanceFrame() {
        return 0;
    }

    @Override
    public int getStanceId() {
        return 0;
    }

    @Override
    public boolean isMoving() {
        return false;
    }

    @Nullable
    @Override
    public CombatGauge getHealthGauge() {
        return null;
    }

    @Nullable
    @Override
    public String getDialogue() {
        return null;
    }

    @Nullable
    @Override
    public Actor getTarget() {
        return null;
    }

    @NonNull
    @Override
    public List<OverheadIcon> getOverheadIcons() {
        return null;
    }

    @Override
    public Coordinate getServerPosition() {
        return null;
    }

    @NonNull
    @Override
    public List<Integer> getSpotAnimationIds() {
        return null;
    }

    @NonNull
    @Override
    public List<Hitsplat> getHitsplats() {
        return null;
    }

    @NonNull
    @Override
    public List<Coordinate> getPath() {
        return null;
    }

    @Override
    public int getPoseId() {
        return 0;
    }

    @Override
    public int getIdlePoseId() {
        return 0;
    }

    @Override
    public boolean isIdle() {
        return false;
    }

    @Nullable
    @Override
    public Coordinate getPosition(Coordinate regionBase) {
        return null;
    }

    @Nullable
    @Override
    public Coordinate.HighPrecision getHighPrecisionPosition(Coordinate regionBase) {
        return null;
    }

    @Nullable
    @Override
    public Area.Rectangular getArea(Coordinate regionBase) {
        return null;
    }

    @Override
    public int getLevel() {
        return 0;
    }

    @Nullable
    @Override
    public NpcDefinition getDefinition() {
        return null;
    }

    @Override
    public int getAnimationId() {
        return 0;
    }

    @Override
    public int getId() {
        return 0;
    }

    @Override
    public boolean isVisible() {
        return false;
    }

    @Override
    public double getVisibility() {
        return 0;
    }

    @Override
    public boolean hasDynamicBounds() {
        return false;
    }

    @Nullable
    @Override
    public InteractablePoint getInteractionPoint(Point origin) {
        return null;
    }

    @Override
    public boolean contains(Point point) {
        return false;
    }

    @Override
    public boolean click() {
        return false;
    }

    @Override
    public boolean interact(@Nullable Pattern action, @Nullable Pattern target) {
        return false;
    }

    @Nullable
    @Override
    public Model getModel() {
        return null;
    }

    @Override
    public void setBackupModel(int[] frontBottomLeft, int[] backTopRight) {

    }

    @Override
    public void setBackupModel(Pair<int[], int[]> values) {

    }

    @Override
    public void setBackupModel(Model backup) {

    }

    @Override
    public void setForcedModel(int[] frontBottomLeft, int[] backTopRight) {

    }

    @Override
    public void setForcedModel(Pair<int[], int[]> values) {

    }

    @Override
    public void setForcedModel(Model forced) {

    }

    @Nullable
    @Override
    public String getName() {
        return null;
    }

    @Override
    public void render(GraphicsContext gc) {

    }

    @Override
    public int getHighPrecisionOrientation() {
        return 0;
    }

    @Override
    public int getOrientationAsAngle() {
        return 0;
    }

    @Override
    public boolean isValid() {
        return false;
    }
}
