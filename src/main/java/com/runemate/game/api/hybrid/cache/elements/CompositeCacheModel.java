package com.runemate.game.api.hybrid.cache.elements;

import com.google.common.hash.*;
import com.runemate.game.api.hybrid.*;
import com.runemate.game.api.hybrid.entities.*;
import com.runemate.game.api.hybrid.local.hud.*;
import com.runemate.game.api.hybrid.location.*;
import com.runemate.game.api.hybrid.projection.*;
import com.runemate.game.api.hybrid.util.shapes.*;
import com.runemate.game.api.osrs.projection.*;
import com.runemate.game.incubating.cache.animation.*;
import java.awt.*;
import java.util.List;
import java.util.*;

public class CompositeCacheModel extends Model {
    private final List<com.runemate.game.api.hybrid.cache.elements.CacheModel> components;
    private boolean scaling, translating, animated;
    private int xScale, yScale, zScale;
    private int xTranslation, yTranslation, zTranslation;
    private int animationId, stanceId;
    private int animationFrame, stanceFrame;

    public CompositeCacheModel(final LocatableEntity entity, final int[] componentIds) {
        this(entity, com.runemate.game.api.hybrid.cache.elements.CacheModel.loadAll(componentIds));
    }

    public CompositeCacheModel(
        final LocatableEntity entity, final int heightOffset,
        final int[] componentIds
    ) {
        this(entity, heightOffset,
            com.runemate.game.api.hybrid.cache.elements.CacheModel.loadAll(componentIds)
        );
    }

    public CompositeCacheModel(
        final LocatableEntity entity,
        final List<com.runemate.game.api.hybrid.cache.elements.CacheModel> components
    ) {
        this(entity, DEFAULT_HEIGHT_OFFSET, components);
    }

    public CompositeCacheModel(
        final LocatableEntity entity, final int heightOffset,
        final List<com.runemate.game.api.hybrid.cache.elements.CacheModel> components
    ) {
        super(entity, heightOffset);
        this.components = components;
    }

    public List<com.runemate.game.api.hybrid.cache.elements.CacheModel> getComponents() {
        return this.components;
    }

    @Override
    public boolean hasDynamicBounds() {
        return true;
    }

    @Override
    public boolean isValid() {
        return !components.isEmpty();
    }

    public void setScale(int x, int y, int z) {
        xScale = 128;
        yScale = 128;
        zScale = 128;
        if (x != xScale || y != yScale || z != zScale) {
            this.xScale = x;
            this.yScale = y;
            this.zScale = z;
            this.scaling = true;
        }
    }

    public int[] getScaleXYZ() {
        return new int[] { xScale, yScale, zScale };
    }

    public int[] getTranslationXYZ() {
        return new int[] { xTranslation, yTranslation, zTranslation };
    }

    public void setAnimations(int animationId, int animationFrame, int stanceId, int stanceFrame) {
        this.animationId = animationId;
        this.animationFrame = animationFrame;
        this.stanceId = stanceId;
        this.stanceFrame = stanceFrame;
        this.animated = true;
    }

    @Override
    public void animate(int animationId, int animationFrame, int stanceId, int stanceFrame) {
        CacheAnimation animation = /*animationId != -1 ? Animations.load(animationId) :*/ null;
        CacheAnimation stance = /*stanceId != -1 ? Animations.load(stanceId) :*/ null;
        if (animation != null && stance != null) {
            animation.apply(this, animationFrame, stance, stanceFrame);
        } else if (animation != null) {
            animation.apply(this, animationFrame);
        } else if (stance != null) {
            stance.apply(this, stanceFrame);
        }
    }

    @Override
    public final List<Triangle> projectTriangles() {
        return projectTrianglesWithin(Projection.getViewport());
    }

    @Override
    public final List<Triangle> projectTrianglesWithin(Shape viewport) {
        final List<Triangle> triangles = new ArrayList<>(100);
        final Coordinate region_base = Scene.getBase();
        final Coordinate.HighPrecision current_hp = getHighPrecisionPosition(region_base);
        if (current_hp != null && region_base != null) {
            int orientation = getHighPrecisionOrientation();
            for (final com.runemate.game.api.hybrid.cache.elements.CacheModel component : components) {
                final int[] xVertices = component.xVertices.clone();
                final int[] yVertices = component.yVertices.clone();
                final int[] zVertices = component.zVertices.clone();
                if (this.scaling) {
                    int xScale = this.xScale;
                    int yScale = this.yScale;
                    int zScale = this.zScale;
                    for (int index = 0; index < xVertices.length; ++index) {
                        if (xScale != 128) {
                            xVertices[index] = (xVertices[index] * xScale) >> 7;
                        }
                        if (yScale != 128) {
                            yVertices[index] = (yVertices[index] * yScale) >> 7;
                        }
                        if (zScale != 128) {
                            zVertices[index] = (zVertices[index] * zScale) >> 7;
                        }
                    }
                }
                if (translating) {
                    for (int index = 0; index < xVertices.length; ++index) {
                        xVertices[index] = xVertices[index] + xTranslation;
                        yVertices[index] = yVertices[index] + yTranslation;
                        zVertices[index] = zVertices[index] + zTranslation;
                    }
                }
                if (this.animated) {
                    animate(animationId, animationFrame, stanceId, stanceFrame);
                }
                if (orientation > 0 && orientation < OSRSProjection.SIN_TABLE.length) {
                    final int sin = OSRSProjection.SIN_TABLE[orientation];
                    final int cos = OSRSProjection.COS_TABLE[orientation];
                    for (int index = 0; index < xVertices.length; ++index) {
                        int storage = zVertices[index] * sin + xVertices[index] * cos;
                        zVertices[index] = zVertices[index] * cos - xVertices[index] * sin;
                        xVertices[index] = storage;
                        xVertices[index] >>= 16;
                        zVertices[index] >>= 16;
                    }
                }
                int x = current_hp.getX() - (region_base.getX() << 7);
                int y = current_hp.getY() - (region_base.getY() << 7);
                Map<String, Object> cache = new HashMap<>();
                int heightOffset = floorHeight;
                int verticeLengths =
                    Math.min(zVertices.length, Math.min(xVertices.length, yVertices.length));
                for (int index = 0; index < component.aIndices.length; ++index) {
                    int aIndex = component.aIndices[index];
                    int bIndex = component.bIndices[index];
                    int cIndex = component.cIndices[index];
                    if (aIndex < verticeLengths
                        && bIndex < verticeLengths
                        && cIndex < verticeLengths) {
                        Point[] points = Projection.multiAbsoluteToScreen(heightOffset,
                            new int[] {
                                x + xVertices[aIndex], x + xVertices[bIndex], x + xVertices[cIndex]
                            },
                            new int[] {
                                yVertices[aIndex], yVertices[bIndex], yVertices[cIndex]
                            },
                            new int[] {
                                y + zVertices[aIndex], y + zVertices[bIndex], y + zVertices[cIndex]
                            }, cache
                        );
                        if (points == null) {
                            continue;
                        }
                        Point a = points[0];
                        Point b = points[1];
                        Point c = points[2];
                        if (a != null && b != null && c != null
                            && (
                            viewport == null ||
                                viewport.contains(a) && viewport.contains(b) && viewport.contains(c)
                        )) {
                            triangles.add(new Triangle(a, b, c));
                        }
                    }
                }
            }
        }
        return triangles;
    }

    @Override
    protected int getTriangleCount() {
        int count = 0;
        for (com.runemate.game.api.hybrid.cache.elements.CacheModel component : components) {
            count += component.aIndices.length;
        }
        return count;
    }

    @Override
    public int getHeight() {
        int minY = Integer.MAX_VALUE, maxY = Integer.MIN_VALUE;
        for (final com.runemate.game.api.hybrid.cache.elements.CacheModel component : components) {
            int[] yVertices = component.yVertices.clone();
            if (this.scaling) {
                int yScale = this.yScale;
                for (int index = 0; index < yVertices.length; ++index) {
                    if (yScale != 128) {
                        yVertices[index] = (yVertices[index] * yScale) >> 7;
                    }
                }
            }
            for (int index = 0; index < component.aIndices.length; ++index) {
                if (yVertices[component.aIndices[index]] < minY) {
                    minY = yVertices[component.aIndices[index]];
                }
                if (yVertices[component.bIndices[index]] < minY) {
                    minY = yVertices[component.bIndices[index]];
                }
                if (yVertices[component.cIndices[index]] < minY) {
                    minY = yVertices[component.cIndices[index]];
                }
                if (yVertices[component.aIndices[index]] > maxY) {
                    maxY = yVertices[component.aIndices[index]];
                }
                if (yVertices[component.bIndices[index]] > maxY) {
                    maxY = yVertices[component.bIndices[index]];
                }
                if (yVertices[component.cIndices[index]] > maxY) {
                    maxY = yVertices[component.cIndices[index]];
                }
            }
        }
        return maxY - minY;
    }

    @Override
    public final boolean isVisible() {
        final Coordinate region_base = Scene.getBase();
        final Coordinate.HighPrecision current_hp = getHighPrecisionPosition(region_base);
        if (current_hp != null && region_base != null) {
            final int orientation = getHighPrecisionOrientation();
            Shape viewport = null;
            for (final com.runemate.game.api.hybrid.cache.elements.CacheModel component : components) {
                final int[] yVertices = component.yVertices.clone();
                final int[] xVertices = component.xVertices.clone();
                final int[] zVertices = component.zVertices.clone();
                if (scaling) {
                    int xScale = this.xScale;
                    int yScale = this.yScale;
                    int zScale = this.zScale;
                    for (int index = 0; index < xVertices.length; ++index) {
                        if (xScale != 128) {
                            xVertices[index] = (xVertices[index] * xScale) >> 7;
                        }
                        if (yScale != 128) {
                            yVertices[index] = (yVertices[index] * yScale) >> 7;
                        }
                        if (zScale != 128) {
                            zVertices[index] = (zVertices[index] * zScale) >> 7;
                        }
                    }
                }
                if (translating) {
                    for (int index = 0; index < xVertices.length; ++index) {
                        xVertices[index] = xVertices[index] + xTranslation;
                        yVertices[index] = yVertices[index] + yTranslation;
                        zVertices[index] = zVertices[index] + zTranslation;
                    }
                }
                if (animated) {
                    animate(animationId, animationFrame, stanceId, stanceFrame);
                }
                if (orientation > 0 && orientation < (OSRSProjection.SIN_TABLE.length)) {
                    final int sin = OSRSProjection.SIN_TABLE[orientation];
                    final int cos = OSRSProjection.COS_TABLE[orientation];
                    for (int index = 0; index < xVertices.length; ++index) {
                        int storage = zVertices[index] * sin + xVertices[index] * cos;
                        zVertices[index] = zVertices[index] * cos - xVertices[index] * sin;
                        xVertices[index] = storage;
                        xVertices[index] >>= 16;
                        zVertices[index] >>= 16;
                    }
                }
                int x = current_hp.getX() - (region_base.getX() << (7));
                int y = current_hp.getY() - (region_base.getY() << (7));
                if (viewport == null) {
                    viewport = Projection.getViewport();
                }
                Map<String, Object> cache = new HashMap<>();
                int verticeLengths =
                    Math.min(zVertices.length, Math.min(xVertices.length, yVertices.length));
                for (int index = 0; index < component.aIndices.length; ++index) {
                    int aIndex = component.aIndices[index];
                    int bIndex = component.bIndices[index];
                    int cIndex = component.cIndices[index];
                    if (aIndex < verticeLengths && bIndex < verticeLengths &&
                        cIndex < verticeLengths) {
                        Point[] points = Projection.multiAbsoluteToScreen(floorHeight,
                            new int[] {
                                xVertices[aIndex] + x,
                                xVertices[bIndex] + x,
                                xVertices[cIndex] + x
                            }, new int[] {
                                yVertices[aIndex],
                                yVertices[bIndex],
                                yVertices[cIndex]
                            },
                            new int[] {
                                zVertices[aIndex] + y,
                                zVertices[bIndex] + y,
                                zVertices[cIndex] + y
                            }, cache
                        );
                        if (points == null) {
                            continue;
                        }
                        Point a = points[0];
                        Point b = points[1];
                        Point c = points[2];
                        if (a != null && b != null && c != null && (
                            viewport == null ||
                                viewport.contains(a) && viewport.contains(b) && viewport.contains(c)
                        )) {
                            return true;
                        }
                    }
                }
            }
        }
        return false;
    }

    @Override
    public BoundingModel getBoundingModel() {
        int minX = Integer.MAX_VALUE, minY = Integer.MAX_VALUE, minZ = Integer.MAX_VALUE, maxX =
            Integer.MIN_VALUE, maxY = Integer
            .MIN_VALUE, maxZ = Integer.MIN_VALUE;
        for (final com.runemate.game.api.hybrid.cache.elements.CacheModel component : components) {
            int[] xVertices = component.xVertices.clone();
            int[] yVertices = component.yVertices.clone();
            int[] zVertices = component.zVertices.clone();
            if (this.scaling) {
                int xScale = this.xScale;
                int yScale = this.yScale;
                int zScale = this.zScale;
                for (int index = 0; index < xVertices.length; ++index) {
                    if (xScale != 128) {
                        xVertices[index] = (xVertices[index] * xScale) >> 7;
                    }
                    if (yScale != 128) {
                        yVertices[index] = (yVertices[index] * yScale) >> 7;
                    }
                    if (zScale != 128) {
                        zVertices[index] = (zVertices[index] * zScale) >> 7;
                    }
                }
            }
            int vertexLengths = xVertices.length;
            for (int index = 0; index < component.aIndices.length; ++index) {
                if (component.aIndices[index] < vertexLengths &&
                    component.bIndices[index] < vertexLengths && component.cIndices[index] <
                    vertexLengths) {
                    if (xVertices[component.aIndices[index]] < minX) {
                        minX = xVertices[component.aIndices[index]];
                    }
                    if (xVertices[component.bIndices[index]] < minX) {
                        minX = xVertices[component.bIndices[index]];
                    }
                    if (xVertices[component.cIndices[index]] < minX) {
                        minX = xVertices[component.cIndices[index]];
                    }
                    if (xVertices[component.aIndices[index]] > maxX) {
                        maxX = xVertices[component.aIndices[index]];
                    }
                    if (xVertices[component.bIndices[index]] > maxX) {
                        maxX = xVertices[component.bIndices[index]];
                    }
                    if (xVertices[component.cIndices[index]] > maxX) {
                        maxX = xVertices[component.cIndices[index]];
                    }
                    if (yVertices[component.aIndices[index]] < minY) {
                        minY = yVertices[component.aIndices[index]];
                    }
                    if (yVertices[component.bIndices[index]] < minY) {
                        minY = yVertices[component.bIndices[index]];
                    }
                    if (yVertices[component.cIndices[index]] < minY) {
                        minY = yVertices[component.cIndices[index]];
                    }
                    if (yVertices[component.aIndices[index]] > maxY) {
                        maxY = yVertices[component.aIndices[index]];
                    }
                    if (yVertices[component.bIndices[index]] > maxY) {
                        maxY = yVertices[component.bIndices[index]];
                    }
                    if (yVertices[component.cIndices[index]] > maxY) {
                        maxY = yVertices[component.cIndices[index]];
                    }
                    if (zVertices[component.aIndices[index]] < minZ) {
                        minZ = zVertices[component.aIndices[index]];
                    }
                    if (zVertices[component.bIndices[index]] < minZ) {
                        minZ = zVertices[component.bIndices[index]];
                    }
                    if (zVertices[component.cIndices[index]] < minZ) {
                        minZ = zVertices[component.cIndices[index]];
                    }
                    if (zVertices[component.aIndices[index]] > maxZ) {
                        maxZ = zVertices[component.aIndices[index]];
                    }
                    if (zVertices[component.bIndices[index]] > maxZ) {
                        maxZ = zVertices[component.bIndices[index]];
                    }
                    if (zVertices[component.cIndices[index]] > maxZ) {
                        maxZ = zVertices[component.cIndices[index]];
                    }
                }
            }
        }
        return new BoundingModel(owner, floorHeight, new int[] { minX, minY, minZ },
            new int[] { maxX, maxY, maxZ }
        );
    }

    @Override
    public int hashCode() {
        final Hasher hasher = Hashing.murmur3_32().newHasher();
        components.forEach(component -> hasher.putInt(component.hashCode()));
        return hasher.hash().asInt();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o instanceof CompositeCacheModel) {
            CompositeCacheModel that = (CompositeCacheModel) o;
            if (xScale == that.xScale && yScale == that.yScale && zScale == that.zScale
                && xTranslation == that.xTranslation && yTranslation == that.yTranslation &&
                zTranslation == that.zTranslation
                && scaling == that.scaling && translating == that.translating) {
                return components.equals(that.components);
            }
        }
        return false;
    }

    @Override
    public String toString() {
        StringBuilder s = new StringBuilder("Model(origin: cache, components: ");
        for (int index = 0, size = components.size(); index < size; index++) {
            CacheModel cmc = components.get(index);
            if (index > 0) {
                s.append(", ");
            }
            s.append(cmc.getId());
        }
        return s.toString() + ')';
    }

    @Override
    public Set<Color> getDefaultColors() {
        Set<Color> colors = new HashSet<>();
        components.forEach(component -> colors.addAll(component.getColors()));
        return colors;
    }

    public void setTranslation(int xTranslation, int yTranslation, int zTranslation) {
        if (xTranslation != 0 || yTranslation != 0 || zTranslation != 0) {
            this.xTranslation = xTranslation;
            this.yTranslation = yTranslation;
            this.zTranslation = zTranslation;
            this.translating = true;
        }
    }
}
